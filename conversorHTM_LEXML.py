#!/usr/bin/env python3
import csv
import logging.config
import os
import re
import shutil
import subprocess
from datetime import datetime
from getpass import getpass
from sys import platform
from typing import Union  # List Dict, Any, Optional, cast

import pypandoc
import xmltodict
import yaml
from unidecode import unidecode

from analiseTextoNormas import ArquiteturaDados
from analiseTextoNormas import TratamentoDasNormas
from observerCodex import Observer

with open('logging.yaml', 'r') as f:
    config = yaml.safe_load(f.read())
    logging.config.dictConfig(config)

logger = logging.getLogger(os.path.splitext(os.path.basename(__file__))[0])


# @todo tratar save_file


class CodexHTMtoDOCX:
    """
        Modulo de conversão dos arquivos legados do site de legislação,  de .htm para formato lexml.
        Para isso, utiliza-se do parser lexml desenvolvido pela prodasen , disponivel em:

        https://github.com/lexml/lexml-parser-projeto-lei-ws

         Ao instanciar esta classe será necessário informar os seguintes atributos:

         a) tipo_norma - com as seguintes opções: 'decreto', 'decreto legislativo' 'decreto-lei',
         'emenda constitucional','lei', 'lei complementar', 'lei delegada', 'medida provisória'

         b) result_directory - o diretório onde serao gravados os arquivos .xml.

         Utilize o método :

         a) .fit_files: tenta a conversão de todos os arquivos do diretório informado
         b) .fit_directories: converte todos os arquivos que estejam nos diretórios informados em um arquivo .csv

    """
    welcome = """
        PROJETO CODEX . Conversor .htm (Portal da Legislacao) -> .xml (Lexml), utilizando-se do parser lexml 
        construido pela equipe prodasem (https://github.com/lexml/lexml-parser-projeto-lei), do qual temos exemplo em 
        :  'https://legis.senado.gov.br/lexml-parser/parse/static/simulador/simulador.html'.
    
    """

    normas_tipos = {
        'decreto': 'decreto', 'decreto legislativo': 'decreto.legislativo', 'decreto-lei': 'decreto.lei',
        'emenda constitucional': 'emenda.constitucional',
        'lei': 'lei', 'lei complementar': 'lei.complementar', 'lei delegada': 'lei.delegada',
        'medida provisória': 'medida.provisoria'
    }

    # endereco_parser = 'https://legis.senado.gov.br/lexml-parser/parse/static/simulador/simulador.html'

    arquivo_docx_intermediario = 'output.docx'

    meses = ['janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho', 'julho', 'agosto', 'setembro', 'outubro',
             'novembro',
             'dezembro']

    def __init__(self, tipo_norma_default: str, result_directory: str):
        """
        :param tipo_norma_default:
        :param result_directory:
        """
        print(self.welcome)
        # warning_head_message = '//////////////////////////// ATENÇÃO //////////////////////////////////////'
        if tipo_norma_default not in self.normas_tipos:
            print("""
                Conversão de  formato .htm para .lexml , utilizando-se do parser lexml desenolvido pelo prodasen: \
            """)
            # tipo_norma_original = tipo_norma
            tipo_norma_default = 'decreto'
            # sys.exit('ajuste adequadamente o atributo tipo_norma. Execução da aplicação abortada')
        logger.debug(f'Construindo: tipo_norma_default={tipo_norma_default}, result_directory={result_directory}')

        # 'Observer' que salva o log do que ocorreu com o arquivo que se tentou parsear ( salva metadados e caminhos)
        self.__log = Observer(savelog=True)

        self.__insucesso: list = []
        self.__result_directory: str = result_directory
        self.__tipo_norma: str = tipo_norma_default
        self.__parsed_directories = []
        self.__ask_for_no_patterns = False
        self.file: str = ''

        self.xml_parseado = ''
        self.__xml_lista_total: list = []
        self.__analiseTExtoNormas = TratamentoDasNormas(False)

        # ET.register_namespace('xsi', "http://www.lexml.gov.br/1.0 ../xsd/lexml-br-rigido.xsd")
        # etree.register_namespace('xsi', "http://www.lexml.gov.br/1.0")
        # etree.register_namespace('xsi', "http://www.w3.org/2001/XMLSchema-instance")
        # etree.register_namespace('xlink', "http://www.w3.org/1999/xlink")
        # etree.register_namespace('', "http://www.lexml.gov.br/1.0")
        # xsi: schemaLocation

    @property
    def parsed_directories(self):
        # explicar
        return self.__parsed_directories

    @property
    def xml_lista_total(self):
        # explicar
        return self.__xml_lista_total

    @property
    def insucesso(self):
        # explicar
        # @todo verificar
        return self.__insucesso

    @property
    def result_directory(self):
        # explicar
        return self.__result_directory

    @result_directory.setter
    def result_directory(self, var):
        # explicar
        self.__result_directory = var

    def __get_metadados_norma(self, directory: str,
                              filename: str,
                              f: str,
                              orgao_origem='presidencia da república',
                              is_norma_legal=True) -> dict:
        """

        Remove os cabecalhos indesejados dos arquivos .htm , identifica o tipo de norma, e o numero ( na epigrafe ) e
        devolve um dict

        :param directory:
        :param filename:
        :param f: str com o conteudo o arquivo .htm
        :param orgao_origem:
        :param is_norma_legal:

        :return: dict ->  metadados = { 'conteudo': resultado_htm_concatenado, 'tipo_norma': tipo_norma,
                'numero': numero, 'ano': ano, 'epigrafe': epigrafe,'arquivo_origem': filename, 'path_origem': directory,
                'orgao_origem': orgao_origem,
                'data_assinatura': data_assinatura if data_assinatura else datetime.today().strftime('%Y-%m-%d'),
                'is_norma_legal': is_norma_legal
                }
        """
        ano: str = ''
        dia: str = ''
        mes: str = ''
        numero: str = ''
        epigrafe: str = ''
        data_assinatura: str = ''
        resultado_htm: list = []
        tipo_norma: str = ''
        f = f.replace("\n", " ")
        f = f.replace(">", ">\n")
        file_list = f.split('\n')

        # variáveis de controle dos loops
        pos_body = False
        pos_epigrafe = False
        pos_numero = False
        pos_ano = False
        is_excerto_epigrafe = False

        for idx, line in enumerate(file_list):

            # ser for um title da página exclui
            if re.search("<?title>", line) and not pos_body:
                continue

            # verifica se ja entrou no body
            if re.search("<body", line) and not pos_body:
                pos_body = True
                resultado_htm.append(line)
                continue

            # checa se é padrão epigrafe
            # tipo_norma = self.__analiseTExtoNormas.get_ordem_norma_from_content(epigrafe)

            excerto: str = line.lower()
            excerto = excerto.replace('.', '').replace('<.*?>', '')
            excerto = re.sub('<.*?>', '', excerto)
            excerto = re.sub('.*?>', '', excerto)
            if not tipo_norma:
                # busca identificar o tipo de norma deste documento, é padrão que as normas comecem sempre pelo seu
                # "titulo" ( que em direito é denominado epígrafe)
                # Exemplo : Decreto 90094, de 02 de fevereiro de 2022
                # neste caso o tipo de norma é : Decreto.
                tipo_norma = self.__analiseTExtoNormas.get_ordem_norma_from_content(excerto)
                self.__tipo_norma = tipo_norma
                # self.__tipo_norma # + '.*?<'

            if tipo_norma:
                is_excerto_epigrafe = True

            if pos_body and is_excerto_epigrafe:
                if not pos_numero:
                    teste_numero = re.search(r"[0-9]+", excerto)
                    if teste_numero:
                        numero = teste_numero.group(0)

                        pos_numero = True

            if pos_body and (not pos_ano and pos_numero):
                teste_dia = re.search('[0-9]{1,2}º? ', excerto)
                if teste_dia:
                    dia: str = teste_dia.group(0).strip()
                    dia = dia.replace('º', '')

                for mes_referencia in self.meses:
                    teste_mes_extenso = re.search(mes_referencia, excerto)
                    if teste_mes_extenso:
                        mes: str = teste_mes_extenso.group(0)

                teste_ano = re.findall('[21][890][0-9]{2}', excerto)
                if teste_ano:
                    ano = teste_ano[-1]
                    pos_ano = True

            if pos_numero and pos_ano:
                if not pos_epigrafe:

                    data_assinatura_expressa = dia + ' DE ' + mes.upper() + ' DE ' + ano
                    data_assinatura = self.__analiseTExtoNormas.get_data_by_expressa(data_assinatura_expressa)

                    # isso pq o parser lexml so interpreta se a string "decreto-lei" tiver o ifem no meio.
                    if self.__tipo_norma == 'decreto lei':
                        tipo_norma_epigrafe = 'decreto-lei'
                    else:
                        tipo_norma_epigrafe = self.__tipo_norma

                    epigrafe = tipo_norma_epigrafe.upper() + ' Nº ' + numero + ', ' + 'DE ' + data_assinatura_expressa
                    line = epigrafe
                is_excerto_epigrafe = False
                pos_epigrafe = True

            # depois do <body> desconsiderar tudo até a epigrafe
            if pos_body and not pos_epigrafe:
                continue

            # else - ou seja em qualquer outro caso nao analisado acima, monte
            resultado_htm.append(line)

        resultado_htm_concatenado = "\n".join(resultado_htm)
        # tipo_norma = self.__analiseTExtoNormas.get_ordem_norma_from_content(epigrafe)

        # retorna um dicionário que será usado em vários momentos no script
        return {
            'conteudo': resultado_htm_concatenado,
            'tipo_norma': tipo_norma,
            'numero': numero,
            'ano': ano,
            'epigrafe': epigrafe,
            'arquivo_origem': filename,
            'path_origem': directory,
            'orgao_origem': orgao_origem,
            'data_assinatura': data_assinatura if data_assinatura else datetime.today().strftime('%Y-%m-%d'),
            'is_norma_legal': is_norma_legal
        }

    def __save_xml(self, xml_com_epigrafe: str, metadados_norma: dict) -> bool:
        """
        Salva a string em xml (lexml)

        :param xml_com_epigrafe: string com a epigrafe já corrigida
        :return: bool
        """
        print('... salvando lexml')
        if not os.path.isdir('lexml'):
            os.mkdir('lexml')

        filename = './lexml/' + self.file + '.xml'
        self.__xml_lista_total.append(xml_com_epigrafe)

        try:
            with open(filename, 'w') as f:
                f.write(xml_com_epigrafe)
            self.__log.register(metadados_norma, 'salvou lexml em fluxo principal')
            f.close()
        except (OSError, SystemError, TypeError):
            print('Não foi possível salvar o arquivo!')
            os.remove(filename)
            self.__insucesso.append(filename)

            return False
        else:
            if os.stat(filename).st_size <= 300:
                return False
            else:
                return True

    @staticmethod
    def include_pos_parser_datas(xml_str: str, epigrafe: str, arquivo_origem='', path_origem='',
                                 data_assinatura='') -> str:
        """
        Inclui o conteudo da variavel epigrafe na tag <Epigrafe> do xml constante na variável xml_str
        # também pode incluir dados de nome e path no caso de arquivos legados
        :param xml_str: string contendo o  XML convertido a partir do arquivo
        :param epigrafe: string com o texto que sera implantado na epigrafe
        :param arquivo_origem:
        :param path_origem:
        :param data_assinatura:
        :return: str contendo o xml com a epigrafe corrigida
        """
        """
        o parser lexml não salva alguns metadados que serão importantes no projeto, principalmente no que tange ao
        legado, contudo o lexml já preve uma tag <Metadados> onde os usuários podem incluir as informações que julgar
        importante para seus projetos. No nosso caso Arquo_origem e path Origem sera util na decisao para subir no banco
        quando se encontrar dois arquivos legaods de mesmo nome ( foram detectados aproximadamente 3000 arquivos de 
        mesmo nome no portal da legislaçao) .
        Data de assinatura sera importante pq a decisao para subir no banco e estabelecer os tipos de conexoes depende
        muito da temporalidade, assim sendo os lexmls deverao ser lançados no banco, do mais antigo para o mais novo.
        Permitindo assim identificar se uma ligaçao esta alterando uma redacao de uma lei antiga ou instituindo um novo
        dispositivo legal.
        """
        print('...incluindo epigrafe ajustada')
        print('xml_str', xml_str)
        root = xmltodict.parse(xml_str)  # fromstring(xml_str)   # ET.fromstring(xml_str)
        root['LexML']['ProjetoNorma']['Norma']['ParteInicial']['Epigrafe']['#text'] = epigrafe
        if arquivo_origem:
            root['LexML']['Metadado']['Arquivo_Origem'] = arquivo_origem
        if path_origem:
            root['LexML']['Metadado']['Path_Origem'] = path_origem
        if data_assinatura:
            root['LexML']['Metadado']['Data_Assinatura'] = data_assinatura
        # xlm_epigrafe = root.findall('.//Epigrafe')
        # root[''] root[2][0][0][0].text = epigrafe

        print('...epigrafe ajustada incluída')
        return xmltodict.unparse(root)  # etree.tostring(root) #'ISO-8859-1'

    def convert_string_docx(self, conteudo_html: str) -> bool:
        """
                converte a string fornecida em um docx retornando um dicionário, retornando True ou False,
                 conforme o resultado

        :param conteudo_html: str do html tratado, limpando o cabeçalho ( Casa Civil , etc...
        :return: bool
        """

        try:
            pypandoc.convert(source=conteudo_html, format='html', to='docx', outputfile=self.arquivo_docx_intermediario)
        except (OSError, RuntimeError):
            return False
        else:
            return True

    def convert_docx_xml(self, metadados_norma: dict, epigrafe: str, norma_tipo='', norma_legal=True) -> \
            Union[str, bool]:
        """
                Se utiliza do parser lexml instanciado localmente, vide repositório :
         https://github.com/lexml/lexml-parser-projeto-lei-ws

        para converter  o arquivo output.docx em .xml (lexml)

        a  epigrafe e  norma tipo, embora sejam conteudos do metadados_norma, estáo vindo separadas por questão de que
        quando se pega do DOU, as vezes esses dados já estão separados no json de origem

        :param metadados_norma:  {
                        'numero': str(numero_norma),
                        'ano': str(dt.year),
                        'tipo_norma': str(ordem_normativa),
                        #'tipo_norma_parser': str(ordem_normativa_parser),
                        'ementa': ementa,
                        'epigrafe': epigrafe,
                        'conteudo': conteudo_tratado,
                        'orgao_origem': orgao_origem,
                        'data_assinatura' : data_assinatura,
                        'is_norma_legal' : is_norma_legal,
                        'arquivo_origem' : unidecode(epigrafe).replace(' ','_').lower() + '_em_' +
                        data_assinatura.replace('-','_'),
                        'path_origem' : 'dou://'
                    }
        :param epigrafe:
        :param norma_tipo:
        :param norma_legal:
        :return:
        """

        # @subprocess.run(['java', '-jar', 'parser_lexml/lexml-parser-projeto-lei-1.12.3-SNAPSHOT-onejar.jar', 'parse',
        # '-t', 'decreto', '-n', '280',
        # '-m', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '--ano', '2021', '-i',
        # 'parser_lexml/exemplo_infralegal.docx'])

        # As três estratégicas consiste em:
        # a) se foi parseado adequadamente para lexml prossegue o sistema
        # b) se parseou mas jogou o conteudo da articulação da norma na ementa, joga tudo para preambulo
        # c) se nao conseguiu parsear,  chama o emulador de parser
        #                 (que criar um parseamento fake identificando apenas epigrafe e urn)

        pathfile = str(os.path.abspath(os.getcwd()))
        pathfile = pathfile + "/" + self.arquivo_docx_intermediario

        if (self.__tipo_norma and norma_legal) and not norma_tipo:
            norma_tipo = self.normas_tipos[self.__tipo_norma]
        elif not norma_legal:
            norma_tipo = 'decreto'

        try:
            temp_lexml = 'temp.lexml'

            # os erros que o parser salva diz respeito aos erros encontrados  no corpo da redaçao que impedem o
            # parseamento, ao se corrigir estes erros ( que em geral sao de legistica) o parser pode ter sucesso em
            # nova tentativa
            error_filename = self.__result_directory + '/' + 'erros_parser_em_' + metadados_norma['arquivo_origem'] + \
                             '.txt'

            processamento = ['java', '-jar', os.environ['PARSER_LEXML_JAR'],
                             'parse',
                             '-t', norma_tipo.replace(' ', '.'),
                             '-n', metadados_norma['numero'],
                             '-m', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                             '--ano', metadados_norma['ano'],
                             '-i', pathfile,
                             '-o', temp_lexml,
                             '--write-errors-to-file', error_filename
                             ]
            print('processamento', processamento)
            subprocess.run(processamento)

            # ha arquivos legatos .htm, no portal da legislacao que estao em ut8 e outros em latin1, so nao se sabe
            # qual a maior prevalencia.
            with open(temp_lexml) as f:  # @todo ou seria melhor utf8?
                # remove o cabeçalho da norma
                lexml_parseado: str = f.read()
                f.close()
            os.remove(temp_lexml)
        except (OSError, RuntimeError):
            print(metadados_norma)
            print("Não foi possível converter este arquivo em lexml, o que deseja fazer?")
            if self.__ask_for_no_patterns:
                while True:
                    decisao = input("[A]bortar a conversão desse arquivo * \n"
                                    "[F]orçar a produção de um 'lexml-fake' \n"
                                    "[H]elp para entender melhor as opções\n"
                                    "Escolha : ").lower().strip()
                    if decisao == 'a':
                        self.__log.register(metadados_norma, 'abortou a tentativa deste arquivo')
                        return False
                    elif decisao == 'f':
                        self.xml_parseado = self.__analiseTExtoNormas.set_lexml_fake(metadados_norma)
                        print('/////////////// lexml fake /////////////////')
                        print(self.xml_parseado)
                        getpass('pressione enter ')
                        shutil.copy(self.arquivo_docx_intermediario, self.__result_directory + '/' +
                                    metadados_norma['arquivo_origem'] + '.docx')

                        self.__log.register(metadados_norma, 'forçou um lexml-fake')
                        return self.xml_parseado
                    elif decisao == 'h':
                        print(self.__analiseTExtoNormas.__doc__)
                        getpass('Pressione ENTER para voltar às opções')
                        continue
                    else:
                        continue
        else:
            self.xml_parseado = self.include_pos_parser_datas(lexml_parseado, epigrafe,
                                                              metadados_norma['arquivo_origem'],
                                                              metadados_norma['path_origem'],
                                                              metadados_norma['data_assinatura']
                                                              )

        if self.xml_parseado:
            lexml = xmltodict.parse(self.xml_parseado)

            # se foi identificado que o parser lexml, apesar do sucesos, lançou tudo em ementa, precisa-se que se ajuste
            # colocando  o conteudo da articulacao da norma em preambulo

            if type(lexml['LexML']['ProjetoNorma']['Norma']['Articulacao']) is None:
                if '#text' not in lexml['LexML']['ProjetoNorma']['Norma']['ParteInicial']['Preambulo']:
                    ementa_text = lexml['LexML']['ProjetoNorma']['Norma']['ParteInicial']['Ementa']['#text']
                    lexml['LexML']['ProjetoNorma']['Norma']['ParteInicial']['Preambulo']['#text'] = ementa_text
                    del lexml['LexML']['ProjetoNorma']['Norma']['ParteInicial']['Ementa']['#text']
                    self.__log.register(metadados_norma, 'dados do lexml ficaram todos no preambulo')
            else:
                self.__log.register(metadados_norma, 'lexml montado perfeitamente')

            return xmltodict.unparse(lexml)
        else:
            return False

    def fit_files(self, directory=".", ask_for_no_patterns=False) -> bool:
        """
        Itera com todos os arquivos .htm  do dirctory apontado no parametero  e faz a conversoã para .xml lexml.

        :param directory: diretório onde se encontram os arquivos a serem parseados
        :param ask_for_no_patterns: bool - se True entao o metodo perguntara se continua tratando cada arquivo que
        ele não tenha encontrado
        :return: bool
        """
        if platform_is_unix_based() and '\\' in directory:
            directory = directory.replace('\\', os.sep)
        logger.debug(f'directory={directory}')
        filename = ''
        counter_sucess: int = 0
        self.__ask_for_no_patterns = ask_for_no_patterns
        for filename in os.listdir(directory):
            if filename.endswith(".htm"):
                self.file = filename  # .htm file
                print("////////////////// PARSEANDO ARQUIVO //////////////////////////////")
                print("tratando o arquivo ", filename)
                filepath = directory + '/' + filename
                with open(filepath, encoding='latin1', errors='replace') as f:  # @todo ou seria melhor utf8?
                    # remove o cabeçalho da norma
                    metadados_norma: dict = self.__get_metadados_norma(directory, filename, f.read())
                    metadados_norma['arquivo_origem'] = filename
                    metadados_norma['path_origem'] = directory

                    self.__log.register(metadados_norma, 'montou metadados normas')

                    if ask_for_no_patterns is True and (metadados_norma['epigrafe'] and metadados_norma['ano']) == '':
                        while True:
                            print("digite A, P ou I para prosseguir")
                            decisao = input("[A]bortar a conversão desse arquivo * \n"
                                            "[P]rosseguir com este arquivo \n "
                                            "[I] inserir manualmente\n"
                                            "Escolha : ")
                            decisao = decisao.lower().strip()
                            if decisao in ('a', 'p', 'i'):
                                break
                        if decisao == 'a':
                            continue
                        elif decisao == 'i':
                            print('Arquivo ' + filename)
                            print('tipos de normas possíveis:')
                            caca_tipo = ArquiteturaDados.normas_padrao_d9191 + ArquiteturaDados.normas_nao_padronizadas
                            caca_tipo = sorted(caca_tipo, key=len)
                            caca_tipo.append('outro')
                            print(caca_tipo)

                            while True:
                                metadados_norma['tipo_norma'] = input(
                                    'insira o tipo de norma para essa norma, caso não '
                                    'insira nenhum o algoritmo entenderá como '
                                    'o modelo padrão').lower().strip()
                                if metadados_norma['tipo_norma'] in caca_tipo:
                                    self.__log.register(metadados_norma, 'incluiu manualmente os metadados')
                                    break

                            metadados_norma['tipo_norma'] = unidecode(metadados_norma['tipo_norma'].lower().strip())
                            metadados_norma['tipo_norma'] = 'decreto' if metadados_norma['tipo_norma'] == 'outro' else \
                                metadados_norma['tipo_norma']

                            metadados_norma['epigrafe'] = input(
                                'insira a EPIGRAFE para essa norma, caso não insira nenhum'
                                ' o algoritmo tentará criar algum com base nos dados: ')
                            metadados_norma['ano'] = input('insira o ANO para essa norma, caso não insira nenhum valor '
                                                           'o algoritmo considerara o ano atual:(sem pontos `.`)  ')
                            metadados_norma['numero'] = input('insira NUMERO para essa norma (sem pontos `.`), '
                                                              'caso não insira nenhum valor '
                                                              'o algoritmo buscara identificar por conta própria: ')

                    resultado = self.convert_string_docx(metadados_norma['conteudo'])
                    if not type(resultado):
                        continue
                    lexml = self.convert_docx_xml(metadados_norma, metadados_norma['epigrafe'],
                                                  metadados_norma['tipo_norma'])

                    if not lexml:
                        self.__log.register(metadados_norma, 'não conseguiu gerar lexml normalmente')
                        shutil.copy(self.arquivo_docx_intermediario, self.__result_directory + '/' + filename + '.docx')

                    conversao: bool = self.__save_xml(lexml, metadados_norma)
                    if conversao:
                        counter_sucess += 1
                        print('...arquivo ' + filename + ' convertido com sucesso')
                    else:
                        self.__insucesso.append(filename)
                        print('não foi possível converter o arquivo' + filename)
                    print("... apagando o .docx")
                    os.remove(self.arquivo_docx_intermediario)

                    print(
                        """
                            Fim do parseamento,
                            
                        """
                    )

                    print('diretório com os arquivos xml: ', self.__result_directory)
                    print('quantidade de arquivos parseados com sucesso: ', counter_sucess)
                    # converter para docx
        print('Arquivo' + filename + ' parseado!')
        return True

    def fit_directories_tree(self, list_directories_filename: str, ask_for_no_patterns=False, path_base='.') -> bool:
        """
         Por base na lista de diretórios disponíveis em um arquivo .csv ( que cotenha coluna única com o endereço dos
        diretórios), trata cada arquivo de cada diretório listado.
        O arquivo .csv deve estar na raiz do diretório onde se encontram os arquivos a serem parseados.

        :param list_directories_filename:  nome do arquivo .csv
        :param ask_for_no_patterns: caso negativo, será  respondida apenas uma lista contendo os xmls
        :param path_base:
        :return: bool (sucesso ou insucesso)
        """

        # path_base = path_base + '/' if path_base else ''
        tmp_file = path_base + "/" + "tmp.csv"
        input_file_name = path_base + '/' + list_directories_filename
        logger.debug(f'input_file_name={input_file_name}, tmp_file={tmp_file}')
        with open(input_file_name, "r") as csv_file, open(tmp_file, "w") as outFile:
            reader = csv.reader(csv_file, delimiter=';')
            writer = csv.writer(outFile, delimiter=',')
            lst_reader = list(reader)
            rows = [i for i in lst_reader if i[1] == '1' and (i[2] == '0' or i[2] == '')]
            for row in rows:
                diretorio = row[0]

                self.fit_files(path_base + '/' + diretorio, ask_for_no_patterns)
                self.__parsed_directories.append(path_base + '/' + diretorio)
                row[2] = '1'
                writer.writerow(row)
        # Código comentado (para que o processamento seja refeito do início a cada chamada):
        # os.rename(tmp_file, input_file_name)
        #
        # TODO: criar outra lógia para que não seja necessário o reprocessamento de arquivos.
        #       Sugestão: gerar outro arquivo com o processamento já realizado de forma
        #       que seja possível verificar o que já foi processado e que o arquivo de entrada
        #       não precise ser sobrescrito.
        return True


def prefix_with_data_dir(app_dir, data_dir=os.getenv('DATA_DIR')):
    if data_dir is not None:
        return data_dir + '/' + app_dir
    else:
        return app_dir


def get_required_env_variables():
    try:
        data_dir = os.environ['DATA_DIR']
        csv_file = os.environ['CSV_FILE']
        htm_dir = prefix_with_data_dir(os.environ['HTM_DIR'])
        parser_lexml_jar = os.environ['PARSER_LEXML_JAR']
        pandoc_bin = os.environ['PANDOC_BIN']
    except KeyError as e:
        logger.fatal('Undefined environment variable: ' + str(e))
        return None
    else:
        return data_dir, csv_file, htm_dir, parser_lexml_jar, pandoc_bin


def parser_lexml_jar_isfile():
    return os.path.isfile(os.environ['PARSER_LEXML_JAR'])


def pandoc_is_executable():
    return os.access(os.environ['PANDOC_BIN'], os.X_OK)


def all_dependencies_are_satisfied():
    parser_lexml_jar_ok = parser_lexml_jar_isfile()
    if not parser_lexml_jar_ok:
        logger.fatal('PARSER_LEXML_JAR is not available!')
    pandoc_bin_ok = pandoc_is_executable()
    if not pandoc_bin_ok:
        logger.fatal('PANDOC_BIN is not available!')
    return parser_lexml_jar_ok and pandoc_bin_ok


def print_required_env_variables(env_variables):
    debug = (f"DATA_DIR={env_variables[0]}; "
             f"CSV_FILE={env_variables[1]};  "
             f"HTM_DIR={env_variables[2]}; "
             f"PARSER_LEXML_JAR={env_variables[3]}; "
             f"PANDOC_BIN={env_variables[4]}")
    logger.debug(debug)


def platform_is_unix_based():
    return platform == 'linux' or platform == 'linux2' or platform == 'darwin'


def main():
    env_variables = get_required_env_variables()
    if env_variables:
        print_required_env_variables(env_variables)
        if all_dependencies_are_satisfied():
            logger.debug('pwd=' + os.getcwd())
            lexml_dir = prefix_with_data_dir('lexml')
            teste_htm_to_lexml = CodexHTMtoDOCX('decreto', lexml_dir)
            _, csv_file, htm_dir, _, _ = env_variables
            teste_htm_to_lexml.fit_directories_tree(csv_file, True, htm_dir)
    else:
        print("TODO: implement code to read command line args!")


if __name__ == "__main__":
    main()
