#!/usr/bin/env bash
BASE_DIR=$(cd "`dirname "$0"`"; pwd)
source "$BASE_DIR"/common.sh

cmd=$(echo "docker container run -it --rm \
	-e "CSV_FILE=lista-diretorios-html.csv" \
	-e "HTML_DIR=CCVIL_03" \
	-v "$DATA_DIR":/data \
	$DOCKER_IMAGE_NAME:$DOCKER_IMAGE_VERSION" /bin/bash)

echo "$cmd $@"

$cmd "$@"
