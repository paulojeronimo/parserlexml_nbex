#!/usr/bin/env bash
#
# Copy this file to config.sh and edit the following variables according to your environment

DOCKER_IMAGE_NAME=${DOCKER_IMAGE_NAME:-orthophilos/parserlexml_nbex}
DOCKER_IMAGE_VERSION=${DOCKER_IMAGE_VERSION:-latest}
