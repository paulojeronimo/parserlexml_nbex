#!/usr/bin/env bash
#
# Copy this file to env-variables.sh.
# Edit the following variables according to your environment

# Directory where the data (loaded or produced) lives
export DATA_DIR=~/data/parserlexml_nbex

# File containing the list of directories that we'll read
export CSV_FILE=lista-diretorios-html.csv

# The directory that contains the directory (and files) referenced by the $CSV_FILE
export HTM_DIR=CCIVIL_03

# The location of the lexml parser JAR (relative to this directory)
export PARSER_LEXML_JAR=lexml-parser-projeto-lei/target/lexml-parser-projeto-lei-1.12.3-SNAPSHOT-onejar.jar

# The location of the pandoc binary
export PANDOC_BIN=/usr/local/bin/pandoc
