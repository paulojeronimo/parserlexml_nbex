#/usr/bin/env bash
set -eou pipefail

cd "$BASE_DIR"

source ../env-variables.sh 2>&- || source ../env-variables.sample.sh
source ./config.sh 2>&- || source ./config.sample.sh
