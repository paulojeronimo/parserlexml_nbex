import re
import datetime
import pickle
# from getpass import getpass
import html2text
import nltk
from collections import OrderedDict

import xmltodict
from unidecode import unidecode
from nltk.corpus import mac_morpho
from nltk.corpus import floresta
from nltk.util import ngrams
from typing import Union, Dict, Any

from sklearn.preprocessing import StandardScaler


# from typing import List, Union


# import time

class ArquiteturaDados:
    """
    list(s) e dict(s) com os padrões de informação a serem usados no conversor e no banco de dados conforme o
    documento Arquitetura_e_Comportamento_de_Dados_Codex

    """
    meses: dict = {"janeiro": "01", "fevereiro": "02", "marco": "03", "março": "03", "abril": "04", "maio": "05",
                   "junho": "06", "julho": "07", "agosto": "08", "setembro": "09", "outubro": "10", "novembro": "11",
                   "dezembro": "12"}

    # vide subcapitulo 2.7.1 Ordens das Classes de Partículas Normativas do documento de Arquitetura e comportamento
    # dos dados
    ordem: list = ['constituicao', 'emenda.constitucional', 'decreto.lei', 'medida.provisoria', 'mensagem.veto',
                   'lei.complementar',
                   'lei', 'decreto', 'resolucao', 'instrucao.normativa', 'portaria', 'deliberacao', 'parecer.tecnico']
    # vide subcapitulo 2.7.2 Famílias das Partículas da Classe das Partículas Normativas, do documento de Arquitetura
    # e comportamento dos dados
    genero: list = ['Parte', 'Livro', 'Titulo', 'Capitulo', 'Secao', 'Subsecao', 'Artigo', 'Caput', 'Paragrafo',
                    'Inciso', 'Alinea', 'Item']

    # Brasil republica so teve capital nessas duas cidades, portanto a assinatura dos atos normativos em geral
    # veio desses lugares

    locais_assinatura: list = ['brasilia', 'rio de janeiro']

    # status possíveis das normas
    status: list = ["Não.Consta.Revogacao.Expressa", "Vigente", "Vacatio.Legis"]

    # vide subcapitulo 2.7.2 Famílias das Partículas da Classe das Partículas Normativas, do documento de Arquitetura
    # e comportamento dos dados
    familia: dict = {
        'Ementa': 'parteInicial',
        'Preambulo': 'parteInicial',
        'Norma': 'agrupadora',
        'Parte': 'agrupadora',
        'Livro': 'agrupadora',
        'Titulo': 'agrupadora',
        'Capitulo': 'agrupadora',
        'Secao': 'agrupadora',
        'Subsecao': 'agrupadora',
        'Artigo': 'agrupadora',
        'Caput': 'articulacao',
        'Paragrafo': 'articulacao',
        'Inciso': 'articulacao',
        'Alinea': 'articulacao',
        'Item': 'articulacao',
        'Fecho': 'partefinal',
        'Anexos': 'anexo'
    }

    # a ser usado no metodo para localizacao da particula no texto
    localizacao_no_texto_model: dict = {
        'agrupamento_principal': {
            'parteInicial': 1,
            'agrupadora': 2,
            'articulacao': 2,
            'parteFinal': 3,
            'Anexos': 4
        },
        'agrupamento_secundario':
            ['Norma', 'Parte', 'Livro', 'Capitulo', 'Secao', 'Subsecao', 'Artigo'],
        'agrupamento_articulacao':
            ['Caput', 'Paragrafo', 'Inciso', 'Alinea', 'Item'],
        'agrupamento_textual':
            ['Id', 'Sequencia']

    }
    # modelo completo de atributos de um nó da classe de particulas normativas
    node_model: dict = {
        "tipoJson": "node",  # tipoJson não é atributo do  banco, mas apenas para ajudar a separar o que é nó de aresta
        # "node_id": "Norma",
        "urnNorma": "",
        "urnParticula": "",
        "classe": "particula normativa",
        "ordem": "",
        "familia": "articulacao",
        "genero": "Caput",
        "especieParticula": "p",
        "abrangencia": "federal",
        "autoridadeOrigem": "federal",
        "codOrgaoOrigem": "",
        "dataFimVigencia": "",  # formato yyyy-mm-dd ISO 8601;
        "dataInicioVigencia": "",  # formato yyyy-mm-dd ISO 8601;
        "dataPublicacao": "",  # formato yyyy-mm-dd ISO 8601;
        "particulaReferenteAInicioVigencia": False,
        "hieraquiaNormasNivel": 5,
        "iniciativaOrigem": "",
        "localizacaoNoTexto": "0@0.0.0.0.0.0.0@0.0.0.0.0@0.0",
        "local": "Brasília",
        "republicado": False,
        "dataRepublicacao": "",
        "divAnteriorRepublicacao": "",
        "status": "nao_consta_revogacao_expressa",  # Documento de arquitetura item 8.2.29 .  valores possiveis :
        # nao_consta_revogacao_expressa, revogado, vacatio_legis
        "status_Secundario": "",
        "sofreuAlteracao": False,
        "curadoriaPendente": 3,  # 0-não; 1-erros parser lexml; 2-muitas reclamações; 3-recem publicado;
                                 # 4 - marcado pelo curador
        "curadorAtribuiuStatusSecundario": "",
        "justificativaStatusSecundario": "", # endereço para documento com justificativa dada pelo curador
        "sujeitoAutoridadeOrigem": "",
        "epigrafe": "",
        "rotulo": "",
        "nomeAgrupador": "",
        "denominacao": "",
        "publicKey": "",
        "div": ""
    }

    node_parte_final_model: dict = {'cidade': "", 'dataAssinatura': "", 'cargoAutoridade': 'Presidente da República',
                                    'nomeSignatario': "", 'localAssinatura': '',
                                    'Referenda': [{'nome': '', 'cargoAutoridade': ''}]}
    # modelo completo de atributos de um nó da classe de pessoas físicas
    node_pessoa_fisica_model: dict = {'codAutoridade': '', 'cpfAutoridade': '', 'nome': '', 'siape': ''}

    # tipos de arestas possiveis entre duas particulas normativas
    type_edges = {
        'geral': ['associacao', 'ontologia', 'instituicao', 'iniciaVigencia', 'encerraVigencia', 'alteracao',
                  'assinatura',
                  'evolucao', 'hierarquizacao'],
        'externo': ['associacao', 'ontologia', 'instituicao', 'iniciaVigencia', 'encerraVigencia', 'alteracao'],
        'interno': ['assinatura', 'evolucao', 'hierarquizacao']
    }
    # pesos,100  - (sequencia fibonacci [0.0, 1, 1, 2, 3, 5, 8, 13,21, 34, 55, 89]/100))
    # o numero 100 foi escolhi

    #atributos necessários para as arestas entre particulas normativas e destas para as demais classes de particulas
    law_link_model: dict = {
        "alteracao": {
            "tipoJson": "aresta",
            # tipoJson não é atributo do banco, mas apenas para ajudar a separar o que é nó do que é aresta
            "tipo": "alteracao",
            "_from": "",  # "particulaAlteradora"
            "_to": "",  # "particulaAlterada"
            "peso": 97
        },
        "associacao": {
            "tipoJson": "aresta",
            # tipoJson não é atributo do banco, mas apenas para ajudar a separar o que é nó do que é aresta
            "tipo": "associacao",
            "_from": "",  # "urnParticulaOrigem"
            "_to": "",  # "particulaDestino"
            "div": "",
            "peso": 45
        },

        "assinatura": {
            "tipoJson": "aresta",
            # tipoJson não é atributo do banco, mas apenas para ajudar a separar o que é nó do que é aresta
            "tipo": "assinatura",
            "_from": "",  # "urnParticulaOrigem"
            "_to": "",  # "nomeAutoridade"
            "tipoAssinatura": "signatario",
            "cpfAutoridade": "",
            "cargoAutoridade": "Presidente da República",
            "codLocalAssinatura": 0,
            "dataAssinatura": "",
            "orgaoAutoridade": "presidenciaDaRepublica",
            "nomeLocalAssinatura": "",
            "partidoPolitico": "",
            "UFOrigem": "DF",
            "Siape": "",
            "publicKey": "",
            "peso": 99

        },

        "encerraVigencia": {
            "tipoJson": "aresta",
            # tipoJson não é atributo do banco, mas apenas para ajudar a separar o que é nó do que é aresta
            "tipo": "encerraVigencia",
            "_from": "",  # "particulaRevogadora"
            "_to": "",  # "particulaRevogada"
            "dataVigenciaEncerrada": "",
            "peso": 98
        },

        "evolucao": {
            "tipoJson": "aresta",
            #  tipoJson não é atributo do banco, mas apenas para ajudar a separar o que é nó do que é aresta
            "tipo": "evolucao",
            "_from": "",  # "particulaAntiga"
            "_to": "",  # "particulaNova"
            "peso": 92
        },
        "hierarquizacao": {
            "tipoJson": "aresta",
            # tipoJson não é atributo do banco, mas apenas para ajudar a separar o que é nó do que é aresta
            "tipo": "hierarquizacao",
            "_from": "",  # filha - urnParticulaOrigem
            "_to": "",  # pai - particulaDestino
            "peso": 87
        },
        "iniciaVigencia": {
            "tipoJson": "aresta",
            # tipoJson não é atributo do banco, mas apenas para ajudar a separar o que é nó do que é aresta
            "tipo": "iniciaVigencia",
            "_from": "",  # urnParticulaAlteradora
            "_to": "",  # urnParticulaAlterada
            "dataVigenciaIniciada": "",
            "peso": 79
        },
        # pesos, sequencia fibonacci /100 = 0.0, 0.01, 0.01, 0.02, 0.03, 0.05, 0.08, 0.13, 0.21, 0.34, 0.55, 0.89

        "instituicao": {
            "tipoJson": "aresta",
            # tipoJson não é atributo do banco, mas apenas para ajudar a separar o que é nó do que é aresta
            "tipo": "instituicao",
            "_from": "",  # particulaInstituidora
            "_to": "",  # particulaInstituida
            "peso": 66
        },
        "ontologica": {
            "tipoJson": "aresta",
            # tipoJson não é atributo do banco, mas apenas para ajudar a separar o que é nó do que é aresta
            "tipo": "hierarquizacao",
            "_from": "",  # urnParticulaOrigem
            "_to": "",  # ontologia
            "peso": 11
        }
    }
    local_assinatura_model: dict = {'Brasilia': 0, 'Rio de Janeiro': 1, 'Outros': 2}

    # modelos de atributos de arestas conforme cada tipo de aresta possível
    # estas normas deveriam seguir o padrao estabelecido pela lei complementar 51/85 e pelo decreto 9191/2017
    normas_padrao_d9191 = ['constituicao', 'lei', 'emenda constitucional', 'decreto lei', 'medida provisoria',
                           'lei complementar', 'decreto', 'decreto sem numero',
                           'decreto nao numerado']  # 'decreto nao numerado'

    # normas com nomes compostos
    normas_compostas = {
        'ato normativo': 'ato_normativo',
        'emenda constitucional': 'emenda_constitucional',
        'decreto lei': 'decreto_lei',
        'decreto numerado': 'decreto',
        'decreto não numerado': 'decreto',
        'medida provisória': 'medida_provisória',
        'mensagem veto': 'mensagem_veto',
        'lei complementar': 'lei_complementar',
        'instrucao normativa': 'instrucao_normativa',
        'parecer tecnico': 'parecer_tecnico'
    }
    #normas que tendem a nao segur o padrao lc51/d9191 e portanto maior probabilidade de insucesso no parser lexml
    normas_nao_padronizadas = [
        'portaria',
        'ato',
        'resolução',
        'instruçao normativa',
        'regimento',
        'mensagem',
        'portaria normativa',
        'ato normativo'
    ]

    numerosPorExtenso = {
        ' zero ': 0, ' um ': 1, ' dois ': 2, ' três ': 3, ' quatro ': 4, ' cinco ': 5, ' seis ': 6, ' sete ': 7,
        ' oito ': 8, ' nove ': 9, ' dez ': 10, ' onze ': 11, ' doze ': 12, ' treze ': 13, ' quatorze ': 14,
        ' quinze ': 15, ' dezesseis ': 16, ' dezessete ': 17, ' dezoito ': 18, ' dezenove ': 19, ' vinte ': 20,
        ' trinta ': 30, ' quarenta ': 40, ' cinquenta ': 50, ' sessenta ': 60, ' setenta ': 70, ' oitenta ': 80,
        ' noventa ': 90, ' cem ': 100, ' cento ': 100, ' duzentos ': 200, ' trezentos ': 300,
        ' quatrocentos ': 400, ' quinhentos ': 500, ' seiscentos ': 600, ' setecentos ': 700, ' oitocentos ': 800,
        ' novecentos ': 900, ' mil ': 1000
    }
    # modelo de lexml para quando se for preciso criar o 'lexml-fake', na impossibilidade de parsear a norma via
    # parser lexml
    lexml_model = """
    <LexML xsi:schemaLocation="http://www.lexml.gov.br/1.0 ../xsd/lexml-br-rigido.xsd" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.lexml.gov.br/1.0">
    <!-- xsi:schemaLocation="http://www.lexml.gov.br/1.0 http://projeto.lexml.gov.br/esquemas/lexml-br-rigido.xsd" > --> 
    <Metadado><Identificacao URN=""/></Metadado><ProjetoNorma><Norma><ParteInicial><Epigrafe id="epigrafe"></Epigrafe>
    <Ementa id="ementa"></Ementa><Preambulo id="preambulo"></Preambulo></ParteInicial><Articulacao><Artigo id="art1">
    </Artigo></Articulacao></Norma></ProjetoNorma></LexML>
    """


class TratamentoDasNormas:
    """
    Classe com tratamentos de expressão regular , técnicas de processamento de linguagem natural e de machine learning
    para tratar excertos dos textos das normas

    """
    # padrao comum de artigos que tratam da entrada em vigor de normas legadas
    vigencia_expressao_padrao = '[eê]s[ts][ea]? .*entr[a|r|á]*? em vig.?r na data de sua publicação.*)|(entra.*? em ' \
                                'vig.*? na data d. sua publicação)'

    genero = ArquiteturaDados.genero

    def __init__(self, load_nlp=True):
        welcome = """
        
        ... carregando algoritmo de analise de textos
              
        """

        print(welcome)

        self.__html2text = html2text.HTML2Text()
        self.__html2text.ignore_images = True
        self.__html2text.ignore_links = True
        self.__html2text.ignore_tables = True

        self.__all_tipo_normas = sorted(ArquiteturaDados.normas_padrao_d9191 + ArquiteturaDados.normas_nao_padronizadas,
                                        key=len)

        self.__json = OrderedDict
        self.__data_publicacao = ''
        self.__data_assinatura = ''

        if load_nlp:
            # preparando as instancias para tratamento de NLP para identificação de entrada em vigencia, alterações
            # normativas e outros dados referentes a ML para identificação de data de inicio de vigencia e
            # relacionamentos entre particulas

            # preparando o POS_tagger das frases
            print('... carregando corpus mac_morpho')
            tagged_sents = mac_morpho.tagged_sents()
            tagger0 = nltk.DefaultTagger('n')
            unigram_tagger = nltk.tag.UnigramTagger(tagged_sents, backoff=tagger0)

            # https://www.linguateca.pt/Floresta/  Anexo 1: Glossário de etiquetas florestais
            print('... carregando corpus floresta')
            tsents = floresta.tagged_sents()
            tagger1 = nltk.UnigramTagger(tsents, backoff=unigram_tagger)

            print('... preparando bigramas')
            self.__tagger2 = nltk.BigramTagger(tsents, backoff=tagger1)
            self.__dict_datas_inicio_vigencia = {}

            print('... preparando stemmer')
            self.__stemmer = nltk.stem.RSLPStemmer()

            # carregando o modelo de machine learning desenovlvido no caderno Jupyter .
            print('... carregando modelo de machine learning: modeloML_identificacao_particula_vigencia.sav')
            self.__model_inicio_vigencia = pickle.load(open('modeloML_identificacao_particula_vigencia.sav', 'rb'))

            print('... carregando modelo de machine learning: modeloML_identificacao_particula_revogadora.sav')
            self.model_revogacao = pickle.load(open('modeloML_identificacao_particula_revogadora.sav', 'rb'))
        # result = loaded_model.score(X_test, Y_test)
        # print(result)
        # clf2 = pickle.loads(s)
        # >>> clf2.predict(X[0:1])

    @property
    def data_publicacao(self):
        return self.__data_publicacao

    @data_publicacao.setter
    def data_publicacao(self, data):
        if self.__validate_date(data):
            self.__data_publicacao = data

    @staticmethod
    def __limpa_div(div: str) -> str:
        div = unidecode(div)
        div = div.lower()

        for numero_extenso, numero_digito in ArquiteturaDados.numerosPorExtenso.items():
            div = div.replace(numero_extenso, ' ' + str(numero_digito) + ' ')

        for norma_separada, norma_underline in ArquiteturaDados.normas_compostas.items():
            div = div.replace(norma_separada, norma_underline)

        div = re.sub(r'\(.*\)', '', div, flags=re.MULTILINE)
        div = div.replace('.', '').replace('  ', ' ').replace(',', '').replace('|', '')
        div = div.replace('(', '').replace(')', '').replace(':', '').replace('-', ' ').replace('º', 'o.')

        return div

    def __traduz_semantica(self, div: str, dict_ontologias: dict, variaveis_chave_identificacao: dict) -> list:
        """
        Lê cada trigrama sequenciado da div transformando-a em uma análise concatenada de semântica (portugues),
        analitica(portugues) e ontologia (regra de negocio). Essa string resultante desta análise será utilizada para a
        regressão lógistica. A análise é feita com trigramas.
        :param div: texto da particula a ser  analisada
        :return: lista com o resultado da análise de cada trigrama
        """

        frase_transformada = []
        lista = []
        tagueados = self.__tagger2.tag(div.split(' '))

        for tagueado in tagueados:
            # print(row, tagueado)
            if len(tagueado[0]) == 0:
                continue
            if self.__stemmer.stem(tagueado[0]) in dict_ontologias:
                # t = tuple([dict_ontologias[stemmer.stem(tagueado[0])]])
                frase_transformada.append(tagueado[1] + '@' + dict_ontologias[self.__stemmer.stem(tagueado[0])])
                # print (tagueado+t)
            else:
                # t = tuple([None])
                frase_transformada.append(tagueado[1])
        n_grams = ngrams(frase_transformada, 3)
        for n_gram in n_grams:
            lista.append(n_gram)
        resultado: list = []

        for var, padrao in variaveis_chave_identificacao.items():
            if padrao in lista:
                resultado.append(1)
            else:
                resultado.append(0)

        scaler = StandardScaler()
        resultado = scaler.fit_transform([resultado])

        # tagueados_final.append([lista, row])
        return resultado

    @staticmethod
    def check_hyperlink(div: str) -> Union[list, bool]:
        """
        Identifica a presença de tags HIPERLINK e cria a associacao entre as duas particulas normativas
        :param div: div a ser analisada
        :return: bool  (False) ou list, neste ultimo caso é o retorno dos  links em html.
        """
        """
        No .htm do legado, por vezes, os editores colocavam links para outra norma relacionada , quando identificavam
        ligacoes relevantes, isso ainda que a norma per si não fazia menção direta àquela outra relacionada.
        
        Isso no parser lexml dai com uma expressao do tipo HYPERLINK <endereço do html> frase tal tal  .
        
        Será bem util conseguir tratar esses pedaços e ja estabeler uma aresta do tipo associacao nestes casos 
        Esse metodo retorna o link dos htmls, para subir no banco vai ter que ter um segundo tratamento para identifcar
        de que outra norma o link se refere
        """
        padrao_regexp = 'hyperlink.*?/.*\.htm'
        m = re.compile(padrao_regexp, re.I | re.MULTILINE)
        hyperlinks = []

        finds = re.findall(m, div)
        if len(finds) == 0:
            return False
        else:
            for find in finds:
                # evoluir isso no futuro, o segundo item da tupla deveria ser o texto do hyperlink
                hyperlinks.append((find, find))

            return hyperlinks

    def __check_inicio_vigencia_regex(self, div: str) -> Union[str, bool]:
        """
        Verfica se a div relativa ao inicio de vigência corresponde a um padrão comum para estes textos - em relação à
        tradição de textos legados - caso positivo, retorna com a data de publicação (data vigência = data publicacao)
        caso negativo retorna falso
        :param div: texto da particula a ser  analisada
        :return: Union [bool,str] = string relativa à data de publicação
        """
        padrao_regexp = r'[eê]s[ts][ea]? .* entr[ará]* em vig.?r na data de sua publicação.*'
        m = re.compile(padrao_regexp, re.I | re.MULTILINE)


        if re.search(m, div):
            # print('deu match!')
            return self.__data_publicacao
        else:
            # print('não deu match!')
            return False

    @staticmethod
    def __validate_date(date_text: str) -> Union[str, bool]:
        """
        Verifica se a string apresenta formato válido de data do tipo  yyyy-mm-dd (ISO 8601)
        :param date_text: string contendo apenas a data
        :return: retorna uma string com a data ou False caso a string fornecida no parâmetro não seja válida
        """

        try:
            datetime.datetime.strptime(date_text, '%Y-%m-%d')
            return date_text
        except ValueError:
            return False

    @staticmethod
    def __soma_numeros(div: str) -> str:
        """
        Considerando que anteriormente  os números por extenso foram convertidos em dígitos, por exemplo :
        'cento e vinte' -> '100 e 20'. Agora, este método faz a somatória destes padrões de string
        convertendo '100 e 20' para 120 e substituindo na div e retornando-a
        :param div: str  - excerto da norma (string a ser avaliada)
        :return: str: excerto com o ajuste dos números somados
        """
        expressao_regular = '[0-9]* e [0-9]*'
        prog = re.compile(expressao_regular, re.I | re.MULTILINE)
        searched = re.search(prog, div)
        if searched:
            padrao_numero = searched.group(0)
            tokens = padrao_numero.split(' ')
            res = []
            for token in tokens:
                if token.isnumeric():
                    res.append(int(token))
            soma = sum(res)
            div = re.sub(prog, str(soma), div)
        return div

    def __get_data_by_prazo(self, div: str) -> Union[str, bool]:
        """
        Verifica se existe substring com o padrão : '[0-9]* (dias|meses)' na string e, em caso positivo, retorna uma
        string de formato data, somando o prazo estabelecido no padrão encontrado à data de publicação da norma.
        :param div: str -  excerto da norma (string a ser avaliada)
        :return: str - data de entrada em vigência predita na str div
        """
        expressao_regular = r'[0-9]* (dias|meses)'
        prog = re.compile(expressao_regular, re.I | re.MULTILINE)
        searched = re.search(prog, div)
        if searched:
            res = searched.group(0)
            ress = res.split()
            resultado = []
            for res in ress:
                if res.isnumeric():
                    resultado.append(int(res))
                elif res == 'meses' or res == 'mês' or res == 'mes':
                    resultado.append(30)
                elif res == 'dias' or ress == 'dia':
                    resultado.append(1)
                elif res == 'anos' or res == 'ano':
                    resultado.append(365)
            dias_apos_publicacao = resultado[0] * resultado[1]

            data = datetime.datetime.strptime(self.__data_publicacao, '%Y-%m-%d')
            data_inicio_vigencia = str(data + datetime.timedelta(days=dias_apos_publicacao))
            return data_inicio_vigencia
        return False

    def __get_valor_data_inicio_vigencia(self, div: str) -> str:
        """
        Por sequencia de expressões regulares capta string com formato de data .

        :param div: str - excerto da norma (string a ser avaliada)
        :return:
        """

        # somando numeros:
        div = self.__soma_numeros(div)

        # ajustando, por padrao, a data de inicio de vigencia à data de publicacao
        data_inicio_vigencia = self.__data_publicacao

        # calculando data de inicio de vigencia a partir de expressões como: (prioridade 1)
        # 'esta norma entra em vigor 180 dias após a publicação' = data_publicação + x dias
        data_by_prazo = self.__get_data_by_prazo(div)
        if data_by_prazo:
            data_inicio_vigencia = data_by_prazo

        # calcula a data de inicio de vigencia conforme expressa no texto (prioridade 2)
        data_by_expressa = self.get_data_by_expressa(div)
        if data_by_expressa:
            data_inicio_vigencia = data_by_expressa

        if self.__validate_date(data_inicio_vigencia):

            return data_inicio_vigencia

        return self.__data_publicacao

    def __check_inicio_vigencia(self, div: str) -> Union[dict, bool]:
        """
        Checa, por expressão regular ou machine learning  se a expressão div, se trata de início da vigência

        :param div:
        :return: data de início de vigência ou falso se na div(str) não possuir data de vigência válida
        """
        # atualmente responde apenas sempre a mesma data para todas as id, mas no futuro vai ser preciso evoluir
        # return {id_particula: inicio_vigencia}

        # checando com expresao regular
        resultado = {}
        data_inicio_vigencia = self.__check_inicio_vigencia_regex(div)

        # pre-tratamento limpando a  div

        div = self.__limpa_div(div)

        for numero_extenso, numero_digito in ArquiteturaDados.numerosPorExtenso.items():
            div = div.replace(numero_extenso, ' ' + str(numero_digito) + ' ')

        for norma_separada, norma_underline in ArquiteturaDados.normas_compostas.items():
            div = div.replace(norma_separada, norma_underline)

        # checando se possui a substring '.*vig.*' ou len (div) > 11
        expressao_regular = '.*vig.*'
        prog = re.compile(expressao_regular)
        searched = re.search(prog, div)
        if not searched or len(div) < 10:
            return False

        # checando com modelo de machine learning
        if not data_inicio_vigencia:
            dict_ontologias = {'constitu': 'NORMA', 'emenda_constituc': 'NORMA', 'códig': 'NORMA', 'consolid': 'NORMA',
                               'decret': 'NORMA', 'decreto_l': 'NORMA', 'medida_provisó': 'NORMA',
                               'mensagem_vet': 'NORMA', 'lei_complement': 'NORMA', 'lei': 'NORMA', 'resolucao': 'NORMA',
                               'instrucao_norm': 'NORMA', 'port': 'NORMA', 'deliber': 'NORMA', 'parecer_tecn': 'NORMA',
                               'vigor': 'VIG', 'part': 'FUT', 'depois': 'FUT', 'decorr': 'FUT', 'após': 'FUT',
                               'jan': 'MES', 'fever': 'MES', 'març': 'MES', 'abril': 'MES', 'mai': 'MES', 'junh': 'MES',
                               'julh': 'MES', 'agost': 'MES', 'setembr': 'MES', 'outubr': 'MES', 'novembr': 'MES',
                               'dezembr': 'MES', 'dia': 'TEMPO', 'mê': 'TEMPO', 'mes': 'TEMPO',
                               'dat': 'TEMPO', 'public': 'PUBLICAÇÃO', 'promulg': 'PUBLICAÇÃO', 'artig': 'PARTICULA',
                               'art.': 'PARTICULA', 'incis': 'PARTICULA', 'inc.': 'PARTICULA', 'capítul': 'PARTICULA'
                               }

            variaveis_chave_identificacao = {
                'var_A': ('V', 'H+prp', 'P<+n'),
                'var_B': ('H+prp', '>N+pron-det', 'H+n@PUBLICAÇÃO'),
                'var_C': ('H+n@TEMPO', 'H+prp', '>N+pron-det'),  # ('H+n@TEMPO', 'H+prp@FUT', '>N+art'),
                'var_D': ('NPROP', 'H+n@NORMA', 'n'),
                'var_E': ('P+v-fin', '>N+art', 'V@VIG'),
                'var_F': ('>N+art', 'V@VIG', 'H+prp')
            }

            div_traduzida = self.__traduz_semantica(div, dict_ontologias, variaveis_chave_identificacao)

            y_predict = self.__model_inicio_vigencia.predict(div_traduzida)

            if y_predict == 1:
                # tratar para pegar a data_inicio_vigencia, no futuro data_inicio_vigencia terá que ser um dict
                # {id: dataInicioVigencia}
                data_inicio_vigencia = self.__get_valor_data_inicio_vigencia(div)

            else:
                return False

        # no futuro ele ira ter que ler a div e encontrar a relacao de quais ids irão entrar em vigor
        resultado.update({'Norma': data_inicio_vigencia})
        return resultado

    @staticmethod
    def __identifica_data(list_frases: list) -> str:
        """
        Identifica um padrão de datas em meio a uma lista de strings e retorna a primeira encontrada
        :param list_frases: List
        :return: string com a data encontrada em padrao %d-%m-%Y
        """

        list_frases.reverse()

        for frase in list_frases:
            datas_encontradas = re.findall('[0-3][0-9]?.[0-9]{1,2}?.[0-2][9,0][0-9]{2}', frase)
            if len(datas_encontradas) != 0:
                data_encontrada = datas_encontradas[0].replace('/', '-').replace('.', '-')
                try:
                    return str(datetime.datetime.strptime(data_encontrada, '%d-%m-%Y').date())
                except ValueError:
                    return str(datetime.date.today())


    @staticmethod
    def __get_numero_norma_from_content(epigrafe: str) -> str:
        """
        Identifica o número da norma, contido em um epigrafe
        :param epigrafe: a string com a epigrafe
        :return: string com o numero da epigrafe ou 's_n' no caso de nao ter identificado numero.
        """
        """
        As normas em geral possuem um número. 
        Ex: A LEI Nº 9.503, DE 23 DE SETEMBRO DE 1997, também conhecida como codigo de transito possui um número, obvio,
        que é o 9503. Esse numero é um parametro para o parser do lexml. Deve-se lembrar que nem todas as possuem numero
        como por exemplo os decretos nao numerados (DNN) muito editados no periodo lula-dilma . Nesses casos, seria 
        impossível parsea-los na atual versao do parser-lemxl (não pelo menos sem 'recursos-tecnicos alternativos')
         
        """
        # limpando a epigrafe dos padroes de data
        for mes in ArquiteturaDados.meses.keys():
            data_extenso_list = re.findall('\d+ de ' + mes + ' de .+', epigrafe)
            epigrafe = epigrafe.replace(data_extenso_list[0], '')

        padrao_numero = re.findall('\d+', epigrafe)

        # se encontrar um numero usa-o, caso contrario retorna s_n (sem numero)
        if len(padrao_numero) > 0:
            return str(padrao_numero[0])
        else:
            return 's_n'

    @staticmethod
    def __urn_solver(metadados_norma: dict) -> str:
        """
        Quando for necessario gerar o lexml-fake , a partir dos metadados fornecidos, gera una URN nos mesmos padroes
        estabelecidos na documentacao do lexml
        :param metadados_norma: { 'conteudo': resultado_htm_concatenado, 'tipo_norma': tipo_norma,
                'numero': numero, 'ano': ano, 'epigrafe': epigrafe,'arquivo_origem': filename, 'path_origem': directory,
                'orgao_origem': orgao_origem,
                'data_assinatura': data_assinatura if data_assinatura else datetime.today().strftime('%Y-%m-%d'),
                'is_norma_legal': is_norma_legal
                }
        :return: str -  URN criada - exemplo: "urn:lex:br:federal:decreto:2021;10610"
        """
        # datetime.datetime.now().microsecond -- esta para
        # impedir que ainda que exista dois arquivos com mesma epigrafe ou que coincidem resultar na mesma urn
        # ele possibilitar que se gere um hash que os diferencie e resulte em uma urn unica.
        return "urn:lex:br:federal:" + metadados_norma['tipo_norma'].replace(' ', '.') + ":" + \
               metadados_norma['ano'] + ";" + metadados_norma['numero'] + '__' + \
               str(datetime.datetime.now().microsecond)

    def data_express_from_pattern(self, div: str) -> str:
        """
        Faz o contrario de get_data_by_expressa, o seja transforma o pubDate="25/01/2021 para 25 de janeiro de 2021

        :param: div - str com a  data_padrao tipo dd/mm/aaaa: pubDate="25/01/2021
        :return: data por expresso , tipo 25 de janeiro de 2021
        """
        div = self.__limpa_div(div)
        # data_inicio_vigencia, aaaa, mm, dd = '0000-00-00', '0000', '00', '00'  # dd/mm/yyyy
        div = div.split('/')
        if len(div) == 3:
            dia = div[0]
            mes = div[1]
            ano = div[2]
        else:
            return ''
        for mes_extenso, mes_numero in ArquiteturaDados.meses.items():
            mes = mes.replace(mes_numero, mes_extenso)

        return dia + ' de ' + mes + ' de ' + ano

    def get_data_by_expressa(self, div: str) -> Union[str, bool]:
        """
        Verifica se há substring relativa a data convertendo para padrão ISO yyyy-mm-dd ISO 8601
        :param div:
        :return:str(data) or bool
        """
        print('get_data_by_expressa', div)
        div = self.__limpa_div(div)
        print('get_data_by_expressa', div)
        data_inicio_vigencia, aaaa, mm, dd = '0000-00-00', '0000', '00', '00'  # yyyy-mm-dd ISO 8601

        expressao_regular = '[0-9]{1,2} de (janeiro|fevereiro|mar[cç]o|abril|maio|junho|julho|agosto|setembro|outubro' \
                            '|novembro|dezembro) de [1-2]{1,2}[0-9]{3}'
        prog = re.compile(expressao_regular, re.I | re.MULTILINE)
        searched = re.search(prog, div)
        print('searched', searched)
        if searched:
            res = searched.group(0)
            ress = res.split()
            print('ress', ress)

            # tratando dia
            if int(ress[0]) <= 31:
                if sum(c.isdigit() for c in ress[0]) == 1:
                    ress[0] = '0' + ress[0]
                dd = ress[0]

            # tratando mes
            for mes, numero in ArquiteturaDados.meses.items():
                if ress[2] == mes:
                    mm = numero
            # tratando ano
            if sum(c.isdigit() for c in ress[-1]) == 4:
                aaaa = ress[-1]

            try:
                data_inicio_vigencia = str(aaaa) + '-' + str(mm) + '-' + str(dd)
                print('data_inicio_vigencia', data_inicio_vigencia)
            except NameError:
                print('data de inicio de vigencia expressa no texto não é válida')
            return self.__validate_date(data_inicio_vigencia)
        return False

    def set_inicio_vigencia(self, sobre_articulacoes: list, inicio_vigencia_superior: dict, is_inicio_vigencia=False):
        """
        ajusta o dict self.dict_datas_inicio_vigencia para chave:id particula valor:entrada em vigor

        :param sobre_articulacoes: lista dos dicts (particulas) que se deve analisar  se se tratam de inicio de vigencia
        :param inicio_vigencia_superior: particula superior
        :param is_inicio_vigencia: como é loop , precisa verificar se o que esta sendo analisado se trata de inicio de
        vigencia
        :return:
        """
        """
        Para o sistema, uma norma é composta por um conjunto de particulas normativas. Cada particula deve ter seu 
        pŕoprio atributo de data de inicio de vigencia, que, embora em regra a data é a mesma para todas as particulas
        de uma norma, algumas vezes ocorrre que alguns artigos, capitulos ou qualquer outra particula tenham datas 
        diferentes de entrada em vigencia ( vide caso da LGPD).
        Este  Metodo faz o primeiro dever de casa, ou seja, seta todas as particulas listadas em sobre_articulacoes
        de aocrdo com o inicio de vigencia identificado no conjunto , para isso ele faz um machine learning para 
        identificar qual string da norma trata do inicio de vigencia , depois identifica e capura essa data de 
        inicio de vigencia e salva um  dicionario (de escopo global), com chave URN(id) e valor a data de inicio
        da norma e a data da articulacao. 
        ATENÇÃO: na versao inicial deste prototipo so capturava a URN/ID 'norma', considerando que toda a norma possuia
        uma unica norma vigente, no futuro isso precisa ser aprimorado.  
        """

        inicio_vigencia: Union[Dict[Any, Any], bool] = inicio_vigencia_superior

        for articulacoes in sobre_articulacoes:

            if type(articulacoes) == OrderedDict:

                for i, (key, value) in enumerate(articulacoes.items()):

                    if key in self.genero:
                        if type(value) == OrderedDict:
                            # id_particula = value['@id']
                            # if '@id' not in value:
                            #    continue
                            if 'p' in value:
                                if type(value['p']) == OrderedDict:
                                    div = value['p']['#text'] if '#text' in value['p'] else ' '
                                else:
                                    div = value['p']
                                inicio_vigencia = self.__check_inicio_vigencia(div)
                                # @todo colocar um check revogacao
                            if type(inicio_vigencia) == dict:
                                is_inicio_vigencia = True
                                # self.__dict_datas_inicio_vigencia.update(inicio_vigencia)
                                self.__dict_datas_inicio_vigencia.update(inicio_vigencia)
                            else:
                                is_inicio_vigencia = False
                                #

                            self.set_inicio_vigencia([value], inicio_vigencia_superior, is_inicio_vigencia)

                        if type(value) == list and all(isinstance(x, OrderedDict) for x in value):
                            subitens_iguais = []
                            for particula in value:
                                subitens_iguais.append(OrderedDict([(key, particula)]))
                            self.set_inicio_vigencia(subitens_iguais, inicio_vigencia_superior, is_inicio_vigencia)

    def check_particula_revogacao(self, div: str) -> bool:
        """
        Checa se a particula esta tratando da revogacao de outra particula

        :param div:
        :return:
        """

        # pre-tratamento limpando a  div

        div = unidecode(div)
        div = div.lower()

        for numero_extenso, numero_digito in ArquiteturaDados.numerosPorExtenso.items():
            div = div.replace(numero_extenso, ' ' + str(numero_digito) + ' ')

        for norma_separada, norma_underline in ArquiteturaDados.normas_compostas.items():
            div = div.replace(norma_separada, norma_underline)

        div = re.sub(r'\(.*\)', '', div, flags=re.MULTILINE)
        div = div.replace('.', '').replace('  ', ' ').replace(',', '').replace('|', '')
        div = div.replace('(', '').replace(')', '').replace(':', '').replace('-', ' ').replace('º', 'o.')

        while True:
            m = re.search(r' {2}', div)
            if m:
                div = re.sub(r' ', '', div, flags=re.MULTILINE)
            else:
                break

        # se não possuir a substring rev ( revogados, revoga-se etc...) então ja retorna falso
        m = re.search(r'.*rev.*', div)
        if not m:
            return False

        # se possuir a substring referente a  expressão( fica(m) revogado(s))
        regexp = r'^fic.* revogad.*'
        prog = re.compile(regexp, re.IGNORECASE)
        if re.search(prog, div):
            return True

        # substituindo : revogam-se disposições em contrário, por marcacao
        expressao_regular = r'revog.*disposições.*?contrário'
        prog = re.compile(expressao_regular)
        div = re.sub(prog, '@REV_CONTRA@', div)

        # criando dados para analise com machine learning
        dict_ontologias = {
            '@REV_CONTRA@': 'ANTI_REVOG', 'constitu': 'NORMA', 'emenda_constituc': 'NORMA', 'códig': 'NORMA',
            'consolid': 'NORMA', 'decret': 'NORMA', 'decreto_l': 'NORMA', 'medida_provisó': 'NORMA',
            'mensagem_vet': 'NORMA', 'lei_complement': 'NORMA', 'lei': 'NORMA', 'resolucao': 'NORMA',
            'instrucao_norm': 'NORMA', 'port': 'NORMA', 'deliber': 'NORMA', 'parecer_tecn': 'NORMA',
            'vigor': 'VIG', 'revogam-s': 'REV', 'revoga-s': 'REV', 'disposi': 'REV', 'jan': 'MES',
            'fever': 'MES', 'març': 'MES', 'abril': 'MES', 'mai': 'MES', 'junh': 'MES', 'julh': 'MES',
            'agost': 'MES', 'setembr': 'MES', 'outubr': 'MES', 'novembr': 'MES', 'dezembr': 'MES',
            'dia': 'TEMPO', 'mê': 'TEMPO', 'mes': 'TEMPO', 'dat': 'TEMPO', 'public': 'PUBLICAÇÃO',
            'promulg': 'PUBLICAÇÃO', 'artig': 'PARTICULA', 'art.': 'PARTICULA', 'incis': 'PARTICULA',
            'inc.': 'PARTICULA', 'capítul': 'PARTICULA'}

        variaveis_chave_identificacao = {'var_A': ('H+prp', '>N+num', 'H+prp'),
                                         'var_B': ('n', 'H+prp', '>N+num'),
                                         'var_C': ('H+n@NORMA', 'ACC+pron-pers', 'n'),
                                         'var_D': ('>N+num', 'H+prp', 'P<+n@MES'),
                                         'var_E': ('H+prp', 'H+num', 'H+prp'),
                                         'var_F': ('n', 'H+prp', 'P<+num'),
                                         # 'var_G': ('H+n', 'NPROP', 'H+n'),
                                         # 'var_H': ('>N+art', 'H+n', 'H+prp'),
                                         # 'var_I': ('>N+art', 'H+n', 'H+prp'),
                                         # 'var_J': ('>N+art', 'H+n', 'H+prp')
                                         }

        div_traduzida = self.__traduz_semantica(div, dict_ontologias, variaveis_chave_identificacao)
        y_predict = self.__model_inicio_vigencia.predict(div_traduzida)

        if y_predict == 1:
            return True

        return False

    @staticmethod
    def get_local_assinatura(div: str) -> str:
        """
        Analisa uma string (do fecho da norma), e retorna o local de assinatura
        :param div: str - fecho com local e data de assinatura
        :return: str: data de assinatura, formato YYYY-MM-DD , %Y-%m-%d
        """
        locais_assinatura = ArquiteturaDados.locais_assinatura

        div = unidecode(div)
        div = div.lower()
        div_splited = div.split(',')
        if div_splited[0] in locais_assinatura:
            return div_splited[0]

        return 'brasilia'

    def fit_norma(self, json: OrderedDict):
        self.__json = json

    def get_inicio_vigencia(self, node_id='Norma') -> str:
        """
        Identifica as datas de início (ou final) de vigência de cada partícula de uma norma e retorna um dicionário
        do tipo {urnParticula0 : dataInicioVigencia, urnParticula1 : dataInicioDeVigencia, ...,
        urnParticulaN : dataInicioDeVigencia  }

        :param node_id:
        :return: str data de inicio de vigencia da norma
        """

        if node_id in self.__dict_datas_inicio_vigencia:
            data_inicio_vigencia = self.__dict_datas_inicio_vigencia[node_id]
        else:
            data_inicio_vigencia = self.__dict_datas_inicio_vigencia['Norma']
        return data_inicio_vigencia

    def get_data_assinatura(self) -> str:

        """
        Coleta a data de assinatura, buscando na seguinte ordem, no XML: 1) metadados; 2) epigrafe 3)  data assinatura
        4) informado pelo usuário
        :return: str - data de assinatura padrão yyyy-mm-dd ISO 8601
        na parte final

        articulacao = json['LexML']['ProjetoNorma']['Norma']['Articulacao']['ParteInicial']['Epigrafe']['#text'] ou
        """

        # setando variaveis com tipagens alternativas
        data_assinatura: Union[str, bool] = False

        # procurar em metadados
        if 'assDate' in self.__json['LexML']['Metadado']:
            data_assinatura = self.__validate_date(self.__json['LexML']['Metadado'])
        # se não encontrar em metadados, procurar em epigrafe
        elif 'Epigrafe' in self.__json['LexML']['ProjetoNorma']['Norma']['ParteInicial']:
            data_assinatura = self.get_data_by_expressa(self.__json['LexML']['ProjetoNorma']['Norma']['ParteInicial']
                                                        ['Epigrafe']["#text"])

            print('epigrafe data assinatura', data_assinatura, )
            data_assinatura = self.__validate_date(str(data_assinatura))
            print('epigrafe data assinatura', data_assinatura, )
        # se não encontrar, procurar em assinatura em
        # json['LexML']['ProjetoNorma']['Norma']['ParteFinal']['LocalData']
        elif 'LocalData' in self.__json['LexML']['ProjetoNorma']['Norma']['ParteFinal']:
            data_assinatura = self.get_data_by_expressa(self.__json['LexML']['ProjetoNorma']['Norma']['ParteFinal']
                                                        ['LocalData'])
            data_assinatura = self.__validate_date(str(data_assinatura))
        # se não encontrar
        else:
            print('Não foi encontrada referência alguma de data de assinatura no XML')
            data_informada_manualmente = input('informe a data de assinatura no  formato yyyy-mm-dd ISO 8601')
            print('data assinatura manual: ', data_assinatura)
            data_assinatura = self.__validate_date(str(data_informada_manualmente))

        self.__data_assinatura = data_assinatura
        if type(data_assinatura) is not str:
            print('self.__data_assinatura = ', self.__data_assinatura, 'data_assinatura', data_assinatura)
            resultado = '0001-01-01'
        else:
            resultado = str(data_assinatura)

        return resultado

    def set_lexml_fake(self, metadados_norma: dict) -> str:

        """
        Caso não tenha sido possível montar um lexml, emula o lexml com base apenas nos parametros
        o lexml de retorno tera urn, ementa, epigrafe e todo o conteudo da norma ficara no preambulo.

        :param metadados_norma: { 'conteudo': resultado_htm_concatenado, 'tipo_norma': tipo_norma,
                'numero': numero,  'ano': ano, 'epigrafe': epigrafe,'arquivo_origem': filename,
                'path_origem' : directory, 'orgao_origem': orgao_origem,
                'data_assinatura': data_assinatura if data_assinatura else datetime.today().strftime('%Y-%m-%d'),
                'is_norma_legal': is_norma_legal
                }

        :return: str com lexml contido
        """

        # re.findall('[0-3]{1}[0-9]?.[0-9]{1,2}?.[0-2]{1}[9,0]{1}[0-9]{2}', c[-1])
        '''
        considerar 
        para pegar o titulo considerar 
        <p class=\"identifica\">DECRETOS DE 29 DE JANEIRO DE 2021</p>
        <title>
            atocnmpv183-04
        </title> 
        
        primeira sequencia de letra maiuscula 
        
        '''

        conteudo = re.sub(r'<script.*</script>', '', metadados_norma['conteudo'], flags=re.MULTILINE | re.I | re.DOTALL)
        # @

        split_in_partes = self.__html2text.handle(conteudo).split('\n')

        if not metadados_norma['tipo_norma']:
            tipo_norma = self.get_ordem_norma_from_content(metadados_norma['epigrafe'])
            if not tipo_norma:
                tipo_norma_from_title = re.findall('<title>.*?</title>', conteudo.lower())
                if tipo_norma_from_title:
                    tipo_norma = tipo_norma_from_title[0]
                else:
                    tipo_norma = metadados_norma['arquivo_origem']
        else:
            tipo_norma = metadados_norma['tipo_norma']

        metadados_norma['data_assinatura'] = self.__identifica_data(split_in_partes)
        metadados_norma['epigrafe'] = metadados_norma['epigrafe'] if metadados_norma['epigrafe'] else \
            self.__gera_epigrafe_fake(tipo_norma, metadados_norma['data_assinatura'])
        metadados_norma['numero'] = metadados_norma['numero'] if metadados_norma['numero'] else \
            self.__get_numero_norma_from_content(metadados_norma['epigrafe'])
        metadados_norma['ano'] = metadados_norma['ano'] if metadados_norma['ano'] else datetime.datetime.now().year
        metadados_norma['tipo_norma'] = tipo_norma
        print('//////////metadados_norma[conteudo]//////////////////', metadados_norma['conteudo'])

        metadados_norma['conteudo'] = self.__html2text.handle(conteudo)

        lexml: dict = xmltodict.parse(ArquiteturaDados.lexml_model)

        if metadados_norma['numero'] and metadados_norma['ano'] and metadados_norma['tipo_norma']:
            lexml['LexML']['Metadado']['Data_Assinatura'] = metadados_norma['data_assinatura']
            lexml['LexML']['Metadado']['Arquivo_Origem'] = metadados_norma['arquivo_origem']
            lexml['LexML']['Metadado']['Path_Origem'] = metadados_norma['path_origem']
            lexml['LexML']['Metadado']['Identificacao']['@URN'] = self.__urn_solver(metadados_norma)
            lexml['LexML']['ProjetoNorma']['Norma']['ParteInicial']['Epigrafe']['#text'] = metadados_norma['epigrafe']
            lexml['LexML']['ProjetoNorma']['Norma']['ParteInicial']['Preambulo']['#text'] = metadados_norma['conteudo']

        return xmltodict.unparse(lexml)

    def get_ordem_norma_from_content(self, epigrafe: str) -> str:  # Union[str, None]:
        """
        Retorna o tipo de norma contida na epigrafe ou retorna vazio
        :param epigrafe:
        :return: ordem da norma. COnsidere Ordem o conceito do documento de arquitetura
        """
        # considerando o lançamento das normas era manual, os editores podem ter digitado normas com acento, sem acento,
        # maiusculo, minusculo, com hifen, sem hifen, etc... a ideia e reduzir a variabilidade.
        epigrafe = unidecode(epigrafe.lower().strip().replace('-', ' '))

        partes_epigrafe = re.compile(' d[eo]s? ').split(epigrafe)

        pre_tipo_norma = partes_epigrafe[0].split(' ')
        possibilidades_tipo_normas = []

        # criando um tipo de range de ngramas, isso pq ha normas de nomenclatura composta que podem se conflitar
        # a exemplo, uma epigrafe que contenha Decreto-Lei pode se confundir com uma que tenha apenas a palavra decreto
        # isso abaixo resolve essa questao, pq no final pega a opçao de tipo de norma que tenha mais caracteres
        for i in range(len(pre_tipo_norma)):
            possibilidades_tipo_normas.append(' '.join(pre_tipo_norma[0:i]))

        tipo_norma = [i for i in possibilidades_tipo_normas if i in self.__all_tipo_normas]

        if tipo_norma:
            # pega a ultima opção, portanto a com o tipo de norma que possua mais caracteres assim sendo pegaria, a
            # exemplo, decreto lei, ao inves de apenas decreto.
            return tipo_norma[-1]
        else:
            return ''

    def __gera_epigrafe_fake(self, tipo_norma, data_assinatura):
        """
        No caso de não ter conseguido gerar o lexml a partir do parser lexml, ao se gerar um lexml, este método gera
        uma epigrafe a partir dos dados que forem possíveis serem identificados no corpo do texto.
        :param tipo_norma: tipo de norma previamente identificado
        :param data_assinatura: data de assinatura previamente identificada
        :return:
        """
        date_time_obj = datetime.datetime.strptime(data_assinatura, '%Y-%m-%d')
        data_expressa = self.data_express_from_pattern(date_time_obj.strftime('%d/%m/%Y'))
        return tipo_norma.upper() + ', ' + data_expressa + 'cod ' + str(datetime.datetime.now().microsecond)
