#!/usr/bin/env bash
set -eou pipefail
cd "`dirname $0`"
d=${0%.build.sh}
d=${d##*/}
[ -d $d ] || git clone https://github.com/lexml/$d
cd $d
mvn clean package
