#!/usr/bin/env bash
BASE_DIR=$(cd "`dirname "$0"`"; pwd)
source "$BASE_DIR"/common.sh

FILES_TO_ADD="`cd ..; echo requirements.txt *.py logging.yaml $PARSER_LEXML_JAR`"

[ -f ../"$PARSER_LEXML_JAR" ] || {
	../lexml-parser-projeto-lei.build.sh || {
		echo "Build failture for $PARSER_LEXML_JAR"
		exit 1
	}
}

d=files-to-add
rm -rf $d
mkdir -p $d
for file in $FILES_TO_ADD
do
	cp ../$file $d/
done

docker image build \
	-f ./Dockerfile \
	--build-arg lexml_jar=${PARSER_LEXML_JAR##*/} \
	-t $DOCKER_IMAGE_NAME:$DOCKER_IMAGE_VERSION .
