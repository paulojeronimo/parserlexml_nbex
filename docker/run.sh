#!/usr/bin/env bash
BASE_DIR=$(cd "`dirname "$0"`"; pwd)
source "$BASE_DIR"/common.sh

cmd=$(echo "docker container run -it --rm \
	-e "CSV_FILE=${CSV_FILE}" \
	-e "HTM_DIR=${HTM_DIR}" \
	-v "$DATA_DIR":/data \
	$DOCKER_IMAGE_NAME:$DOCKER_IMAGE_VERSION" python /code/`basename $0 .sh`.py)

echo "$cmd $@"

$cmd "$@"
