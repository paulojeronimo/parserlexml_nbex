# from getpass import getpass

from analiseTextoNormas import ArquiteturaDados
from typing import List  # , Union, Optional, Dict, Any

from enum import Enum


class AssociacaoSimples(Enum):
    associacao = 'associacao'
    ontologica = 'ontologica'


class EventosDados:

    @staticmethod
    def check_validate_node(node):
        """

        :param node:
        :return:
        """
        if not all(node in key for key in list(ArquiteturaDados.node_model.keys())):
            return False
        else:
            return True

    @staticmethod
    def __set_assinatura(node_origem, node_destino, tipo_assinatura):
        """
        Regras negociais descritas no capítulo 3 do documento Arquitetura_e_Comportamento_de_Dados_Codex
        node_origem = self.nodeParteFinal
        :param node_origem: dict
        :param node_destino: dict
        :param tipo_assinatura
        :return:
        """

        #  a chave Referenda é uma list de dicts com nome e cargo
        # Associa o node ao signatario identificado no metodo nodeParteFInal
        aresta = ArquiteturaDados.law_link_model[tipo_assinatura]
        aresta['tipo'] = tipo_assinatura
        aresta['tipoAssinatura'] = "signatario"
        aresta['codLocalAssinatura'] = node_origem['localAssinatura']
        aresta['cargoAutoridade'] = node_origem['cargoAutoridade']
        aresta['data_assinatura'] = node_origem['data_assinatura']
        aresta['nome'] = node_destino['nome']
        return aresta

    @staticmethod
    def set_relacao_hierarquia_particulas(node_origem: str, node_destino: str) -> list:
        """

        :param node_origem:
        :param node_destino:
        :return:
        """
        tuplas_tratadas = []

        aresta = ArquiteturaDados.law_link_model['hierarquizacao'].copy()
        aresta['tipo'] = 'hierarquizacao'
        aresta['_from'] = node_origem
        aresta['_to'] = node_destino
        tuplas_tratadas.append(aresta)

        return tuplas_tratadas

    @staticmethod
    def set_relacao_entre_normas(node_origem: dict, nodes_destino: List[dict], tipos: AssociacaoSimples, div='') \
            -> List[dict]:
        """
        Regras negociais descritas no capítulo 3 do documento Arquitetura_e_Comportamento_de_Dados_Codex

        :param node_origem:
        :param nodes_destino:
        :param tipos:
        :param div:
        :return:
        """
        # print('entrou em link de associacao')

        # tipos possiveis para este método
        # tipos = ['associacao', 'ontologica', 'hierarquizacao']
        # if tipo not in tipos:
        #    return False
        tipo = str(tipos)
        tipo = tipo.split('.')
        tipo = tipo[1]
        tuplas_tratadas = []

        for node_destino in nodes_destino:
            aresta = ArquiteturaDados.law_link_model[tipo].copy()
            aresta['tipo'] = tipo
            aresta['_from'] = node_origem['urnParticula']
            aresta['_to'] = node_destino['urnParticula']
            caresta['div'] = div
            tuplas_tratadas.append(aresta)
        return tuplas_tratadas

    @staticmethod
    def set_evolucao_de_particulas(node_nova_versao: dict, urn_node_alteradora: str,
                                   urn_node_versao_antiga: str) -> List[dict]:
        """

        Regras negociais descritas no capítulo 4.3 do documento Arquitetura_e_Comportamento_de_Dados_Codex

        :param node_nova_versao: dict
        :param urn_node_alteradora: str
        :param urn_node_versao_antiga: str
        :return: list concatenação de dicts [ node_destino: dict , lawlink: dict ]
        """
        '''
        ATENÇÃO: Importante dizer que analisando apenas o  .XML padrão LEXML é impossível dizer se o evento é uma 
        instituição ou evolução de partícula, a diferenciação entre um evento e outro só se pode dar através da análise 
        se existe partícula anteriormente registrada no banco de dados. 
        '''

        '''
        No banco a nova partícula  herda da antiga  - salvo disposição contrária -   os seguintes valores de atributos
        e os seguintes relacionamentos :
        atributos : i) urnNorma; ii) classe; iii) __ordem; iv) familia; v) genero; vi) especie; vii)abrangencia; viii)
        autoridadeOrigem; ix)codOrgaoOrigem; x) hieraquiaNormasNivel; xi) local; xii) rotulo; e xiii) nomeAgrupador
        Relacionamentos :
               hierarquizacao
               seguir
               assunto
               
        '''

        '''
        Há tres particulas, a origem, a nova que foi criada e a antiga, que vai deixar de vigir. Então devemos: 
        '''
        '''
        a) criar um dict referente à particula antiga (urn_node_versao_antiga). No banco esse no ja existe mas os
         valores dos atributos serão atualizados para o valor de atributo deste dict aqui criado.). 
        a1 - node["dataFimVigencia"] = a data de fim  de vigencia da node_versao_antiga 
        a2 - status = 'revogado'
        '''
        todas_arestas = ArquiteturaDados.node_model
        node_versao_antiga = todas_arestas.copy()
        # node_versao_antiga = ArquiteturaDados.node_model.copy()
        node_versao_antiga["urnNorma"] = node_nova_versao["urnNorma"]
        node_versao_antiga['urnParticula'] = urn_node_versao_antiga
        node_versao_antiga['dataFimVigencia'] = node_nova_versao['dataInicioVigencia']
        node_versao_antiga['status'] = 'revogado'

        '''
        b) criar um dict referente à nova particula (node_nova_versao).  
        Não precisa pois é o próprio parâmetro node_nova_versao
        '''
        '''
        c) estabelecer  relação entre a nova particula  e a particula  instituidora do tipo 'instituicao'        
        '''
        aresta_instituicao = ArquiteturaDados.law_link_model['instituicao'].copy()
        aresta_instituicao['_from'] = urn_node_alteradora
        aresta_instituicao['_to'] = node_nova_versao['urnParticula']

        '''
        d) estabeelcer relação entre a particula antiga (urn_node_versao_antiga)  e a particula nova(node_nova_versao)
        do tipo 'evolucao'        
        '''
        aresta_evolucao = ArquiteturaDados.law_link_model['evolucao'].copy()
        aresta_evolucao["_from"] = urn_node_versao_antiga
        aresta_evolucao["_to"] = node_nova_versao['urnParticula']

        '''
        e) estabelecer uma relação entre a particula antiga (urn_node_versao_antiga) e a paticula alteradora
        (urn_node_alteradora) do tipo 'alteracao' 
        '''
        aresta_alteracao = ArquiteturaDados.law_link_model['alteracao'].copy()
        aresta_alteracao["_from"] = urn_node_alteradora
        aresta_alteracao["_to"] = urn_node_versao_antiga

        return [node_versao_antiga, node_nova_versao, aresta_instituicao, aresta_evolucao, aresta_alteracao]

    @staticmethod
    def set_revogacao_de_particula(particula_revogadora: dict, particulas_revogadas: List[dict]) -> List[dict]:
        """
        Regras negociais descritas no capítulo 4 do documento Arquitetura_e_Comportamento_de_Dados_Codex

        :param particula_revogadora: dict
        :param particulas_revogadas: List[dict]
        :return: List[dict]
        """
        nodes_tratados = []

        for particula_revogada in particulas_revogadas:
            aresta_encerra_vigencia = ArquiteturaDados.law_link_model["encerraVigencia"].copy()
            aresta_encerra_vigencia["_from"] = particula_revogadora['urnParticula']
            aresta_encerra_vigencia["_to"] = particula_revogada['urnParticula']
            aresta_encerra_vigencia["dataVigenciaEncerrada"] = particula_revogadora['dataInicioVigencia']
            nodes_tratados.append(aresta_encerra_vigencia)

            particula_revogada['tipoJson'] = 'node'
            particula_revogada['status'] = 'revogado'
            particula_revogada['dataFimVigencia'] = particula_revogadora['dataInicioVigencia']

        return particulas_revogadas + nodes_tratados
