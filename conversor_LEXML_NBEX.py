import json
import re

import xmltodict
import numpy as np
import os
from analiseTextoNormas import ArquiteturaDados
from analiseTextoNormas import TratamentoDasNormas
from eventosNormas import AssociacaoSimples
from eventosNormas import EventosDados
from typing import List  # , Union, Dict, Any, Optional, cast
from collections import OrderedDict


# java -jar parser_lexml/lexml-parser-projeto-lei-1.12.3-SNAPSHOT-onejar.jar parse -t decreto -n 280  -m
# application/vnd.openxmlformats-officedocument.wordprocessingml.document --ano 2021
# -i parser_lexml/exemplo_infralegal.docx

# from typing import Dict, Any, Union
# subprocess.run(['java', '-jar', 'parser_lexml/lexml-parser-projeto-lei-1.12.3-SNAPSHOT-onejar.jar', 'parse',
# '-t', 'decreto', '-n', '280',  '-m', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
# '--ano', '2021', '-i', 'parser_lexml/exemplo_infralegal.docx'])

class CodexXMLtoDB:
    """
        Converte do modelo lexml para os jsons necessarios para no e arestas


        OBSERVACOES:
        O algoritmo esta em 'monotonico'  ou seja, ele entende que cada particula pode atender apenas de um evento
        ou instituir uma das relaçeõs no banco (cap 3 e 4 do Documento de Arquitetura), futuramente isso precisará
        ser melhorado.

        Versão :  0.1
        Desenvolvido por : Jeferson Tadeu de Souza
        Ano : 1/2021
    """

    def __init__(self):
        welcome = """
        ****************************************************************
        PROJETO CODEX
        -------------------------------------
        Conversor de xml (foramto LEXML) para NBEX ( list[dict]). O presente conversor pretende converter os arquivos 
        xml em formato nbex (list[json]) conforme preconizado no arquivo.
        Para saber mais consulte o documento Arquitetura_e_Comportamento_de_Dados_Codex v0.1.8.pdf
        
        Tutorial básico: 
        
        CodexXMLtoDB.fit_norma(filename: str, filetype="xml"):  Metodo chamado para gerar todos os nos da norma contida 
        no XML
        CodexXMLtoDB.xml_base - pega a URN base da norma a ser analisada
        CodexXMLtoDB.tuplas_nodes - tuplas referentes aos nos da norma analisada
        CodexXMLtoDB.tuplas_edges - tuplas referentes a arestas da norma analisada
        CodexXMLtoDB.tuplas - todas as tuplas da norma analisada
        ****************************************************************
        """
        print(welcome)

        # classe com os algoritmos de machine learning, data mining e o necessário para interpretar os dados contidos
        # nos textos das normas
        self.__tratamentoDasNormas = TratamentoDasNormas()
        # setando
        # aqui estará o produto final da classe, é isso que ira para o db,
        # dividido entre nós e arestas
        self.__tuplas = []
        self.__node_pessoas_fisicas = {}
        self.__aresta_assinaturas = {}  # cada particula deverá estar ligada com a conexão assinatura a cada signatário
        self.nodeParteFinal = ArquiteturaDados.node_parte_final_model.copy()
        self.__nodeModel = ArquiteturaDados.node_model.copy()

        self.__xmlbase = ''

        self.__xmlbase_alterada = ''
        self.__node_anterior = {}

        self.__data_assinatura = ''
        self.__ordem = str
        self.__json = OrderedDict
        self.__xmlbase_alteradora = ''

    @property
    def xml_base(self):
        return self.__xmlbase

    @property
    def tuplas_nodes(self):
        nodes = []
        for value in self.__tuplas:
            if value['tipoJson'] == 'node':
                nodes.append(value)
        return nodes

    @property
    def tuplas(self):
        return self.__tuplas

    """
    @tuplas.setter
    def tuplas(self, var):
        self.__tuplas = var
    """

    @property
    def json(self):
        return self.__json

    """
    @json.setter
    def json(self, var):
        self.__json = var
    """

    # @todo inserir umas funções para eventos

    def __set_json(self, filename: str, filetype="xml"):
        if filetype.lower() == 'xml':
            # procedimentos para xml
            try:
                with open(filename, encoding='utf-8') as f:
                    conteudo = f.read()

                    novo_conteudo = re.sub(r'(HYPERLINK.*?)(\".*\.htm\")(.*)', r'<span xlink:href=\2>\3</span>', conteudo,
                                           flags=re.DOTALL)

                    # padrao encontrado no grep que atingiu
                    # 88,78% das normas   = 'vigor[a-z]* *na *data *de *sua *publica.*'

                    # como cada particula pode ter datas de vigência diferente, é necessária em dicionário
                    # urnparticula:dataInicioVigencia espeficicando cada particula do xml
                    if len(conteudo) > 500:
                        self.__json = xmltodict.parse(novo_conteudo)
            except OSError:
                print("não foi possível abrir o arquivo")
                exit(1)
        elif filetype.lower() == '__json':
            try:
                with open(filename, encoding='utf-8') as f:
                    conteudo = f.read()
                    # padrao encontrado no grep que atingiu
                    # 88,78% das normas   = 'vigor[a-z]* *na *data *de *sua *publica.*'
                    novo_conteudo = re.sub('HYPERLINK.*?<', r'<', conteudo, flags=re.M)

                    # padrao encontrado no grep que atingiu
                    # 88,78% das normas   = 'vigor[a-z]* *na *data *de *sua *publica.*'
                    if len(conteudo) > 500:
                        self.__json = xmltodict.parse(novo_conteudo)
            except OSError:
                print("não foi possível abrir o arquivo")
                exit(1)
        else:
            print("É necessário escolher, no terceiro argumento desta classe, entre xml ou __json")
            exit(1)

        self.__tratamentoDasNormas.fit_norma(self.__json)

        # self.__nodeModel["dataInicioVigencia"] = self.__tratamentoDasNormas.get_inicio_vigencia('Norma')
        #    nodeParteFinalModel = {'cidade': "", '__data_assinatura' : "", 'cargoAutoridade': '','nomeSignatario' :"",
        #    'localAssinatura' : '' ,  'Referenda' : [{'nome': '','cargoAutoridade': ''}]}

        # urn da particula que esa sendo alterada
        pre_xmlbase = self.__json['LexML']['Metadado']['Identificacao']['@URN'] + '@seguranca'
        pre_xmlbase = pre_xmlbase.split('@')
        pre_xmlbase = str(pre_xmlbase[0])
        self.__xmlbase = pre_xmlbase

        self.__data_assinatura = self.__tratamentoDasNormas.get_data_assinatura()
        self.__data_publicacao = self.__json['LexML']['Metadado']['pubDate'] \
            if 'pubDate' in self.__json['LexML']['Metadado'] else self.__data_assinatura

        self.__tratamentoDasNormas.data_publicacao = self.__data_publicacao

        # self.abrangencia = abrangencia
        # self.autoridadeOrigem = autoridadeOrigem

    @staticmethod
    def __get_sequencia_agrupamento_na_posicao(node: dict,
                                               localizacao_no_texto_anterior: list,
                                               tipo_agrupamento: str) -> list:
        """
        Identifica a sequencia do nó relativammente a  agrupamento secundario ou   agrupamentoDeArticulacao, conforme
        o  subcapítulo  8.2.22 - Atributo : LocalizacaoNoTexto.

        :param node: no atual
        :param localizacao_no_texto_anterior: um list com a localizacao do texto anterior.
        :param tipo_agrupamento: que pode ser 'agrupamentoSecundario' ou 'agrupamentoDeArticulacao'
        :return: retorna um np.array com a sequencia identificada
        """

        localizacao_no_texto_model = ArquiteturaDados.localizacao_no_texto_model.copy()

        localizacao_no_texto_anterior = [int(x) for x in localizacao_no_texto_anterior.split(".")]  # .split(".")

        if node['genero'] in localizacao_no_texto_model[tipo_agrupamento]:
            numero_genero = localizacao_no_texto_model[tipo_agrupamento].index(node['genero']) - 1
        elif tipo_agrupamento == 'agrupamento_secundario':
            numero_genero = len(localizacao_no_texto_model['agrupamento_secundario']) - 1
        else:
            numero_genero = 0
        agrupamento_identidade = [0] * len(localizacao_no_texto_model[tipo_agrupamento])
        agrupamento_identidade[numero_genero] = 1

        np_agrupamento_identidade = np.array(agrupamento_identidade) + np.array(localizacao_no_texto_anterior)
        np_agrupamento_identidade2 = np_agrupamento_identidade.copy()

        for i in range(len(np_agrupamento_identidade2)):
            if i <= numero_genero - len(localizacao_no_texto_model[tipo_agrupamento]):
                np_agrupamento_identidade2[i] = 1

        # np_agrupamento_identidade = np.array(localizacao_no_texto_anterior) * np.array(np_agrupamento_identidade2)

        resultado = np_agrupamento_identidade2 + np.array(agrupamento_identidade)
        resultado_lst: list = resultado.tolist()
        return resultado_lst

    def __find(self, key: str, dictionary: dict):
        """
        para encontrar valores de determinadas chaves em qualuer nível nos dict
        :param key:
        :param dictionary:
        :return:
        """

        for k, v in dictionary.items():
            if k == key:
                yield v
            elif isinstance(v, dict):
                for result in self.__find(key, v):
                    yield result
            elif isinstance(v, list):
                for d in v:
                    for result in self.__find(key, d):
                        yield result

    def __check_links_na_particula(self, node: dict) -> List[dict]:
        """
        Verifica se dentro div possui um link para outra partícula, para que possa ser criada uma aresta do tipo
        associacao  se possui retorna-o, se não retorna False

        :param node:
        :return: Node (dict) ou False
        """
        # @todo resolver problema do hyperlink

        # print('node in __check_links_na_particula', type(node),node)
        # getpass("Press Enter to Continue")
        is_hyperlinks: list

        nodes_destinos_associacao = []
        if 'p' in node:
            # print('tem p ')
            if (type(node['p']) == OrderedDict) and ('span' in node['p']):
                spans = node['p']
                if type(spans) == list:
                    for span in spans:
                        node_destino_associacao = {
                            'urnParticula': span['@xlink:href'], 'div': span['#text']}

                        # verifica se há hyperlinks para outras páginas no texto
                        is_hyperlinks = self.__tratamentoDasNormas.check_hyperlink(span['#text'])
                        if is_hyperlinks:
                            for is_hyperlink in is_hyperlinks:
                                nodes_destinos_associacao.append({'link_htm': is_hyperlink[0], 'div': is_hyperlink[1]})

                        nodes_destinos_associacao.append(node_destino_associacao)
                elif type(spans) == OrderedDict:
                    if type(spans['span']) == list:
                        for span in spans['span']:
                            url_link = str(span['@xlink:href'])
                            text_link = str(span['#text'])
                            nodes_destinos_associacao.append({'urnParticula': url_link, 'div': text_link})
                            is_hyperlinks = self.__tratamentoDasNormas.check_hyperlink(text_link)
                            if is_hyperlinks:
                                for is_hyperlink in is_hyperlinks:
                                    nodes_destinos_associacao.append(
                                        {'link_htm': is_hyperlink[0], 'div': is_hyperlink[1]})
                    elif type(spans['span']) == OrderedDict:
                        nodes_destinos_associacao.append({'urnParticula': spans['span']['@xlink:href'],
                                                          'div': spans['span']['#text']})
                        is_hyperlinks = self.__tratamentoDasNormas.check_hyperlink(spans['span']['@xlink:href'])
                        if is_hyperlinks:
                            for is_hyperlink in is_hyperlinks:
                                nodes_destinos_associacao.append(
                                    {'link_htm': is_hyperlink[0], 'div': is_hyperlink[1]})
                    # verifica se há hyperlinks para outras páginas no texto

            elif type(node['p']) == list:
                # print('node p é uma lista')
                # getpass("Press Enter to Continue")

                for sub_node in node['p']:
                    if (type(sub_node) == OrderedDict) and ('span' in sub_node):
                        url_link = sub_node['@xlink:href']
                        text_link = sub_node["#text"]
                        node_destino_associacao = {'urnParticula': url_link, 'div': text_link}
                        nodes_destinos_associacao.append(node_destino_associacao)

                        # verifica se há hyperlinks para outras páginas no texto
                        is_hyperlinks = self.__tratamentoDasNormas.check_hyperlink(sub_node['#text'])
                        if len(is_hyperlinks) > 0:
                            for is_hyperlink in is_hyperlinks:
                                nodes_destinos_associacao.append({'link_htm': is_hyperlink[0], 'div': is_hyperlink[1]})

                # print('##############retornando node 2#####################')
                # getpass("Press Enter to Continue")
            else:
                return [node]
            return nodes_destinos_associacao
        return [node]

    def __include_arestas_assinatura(self, urn_particula: str):
        """

        :param urn_particula:
        :return:
        """

        for assinatura in self.__aresta_assinaturas:
            assinatura["_from"] = urn_particula
            self.__tuplas.append(assinatura)

    def __monta_particula(self, parte_articulacao: dict, key: str, urn_no_superior: str = 'Norma',
                          is_alteracao=False, is_revogacao=False) -> dict:

        """
        Pega o trecho do dict e  e monta um node (nó) a partir dele

        :param parte_articulacao: pedaço do XML a ser analisado
        :param key: tag do XML a ser analisada.
        :param urn_no_superior:  id do  nó  hierarquicamente superior (vide subcapitulo 3.7 do
        Documento de Arquitetura)
        :param is_alteracao: se a partícula em contato é do tipo Alteração
        :return:
        """

        node = self.__nodeModel.copy()

        if key != 'Alteracao':
            node["urnNorma"] = self.__xmlbase
            node["ordem"] = self.__ordem
            node["familia"] = ArquiteturaDados.familia[key]
            node["genero"] = key
            node["especieParticula"] = 'p' if 'p' in parte_articulacao else 'outras tags'
            node["rotulo"] = parte_articulacao["Rotulo"] if 'Rotulo' in parte_articulacao else ''
            node["nomeAgrupador"] = parte_articulacao["nomeAgrupador"] if 'nomeAgrupador' in parte_articulacao else ''
            node["denominacao"] = parte_articulacao["denominacao"] if 'denominacao' in parte_articulacao else ''
            node["epigrafe"] = parte_articulacao["epigrafe"] if 'epigrafe' in parte_articulacao else ''

            node_id = parte_articulacao['@id']

            node["urnParticula"] = self.__xmlbase + "!" + node_id

            #  alterando os atributos '_from' de cada subdicionario dentro  de self.__aresta_assinaturas
            self.__include_arestas_assinatura(node["urnParticula"])

            node["localizacaoNoTexto"] = self.__localiza_posicao(node)
            node["dataInicioVigencia"] = self.__tratamentoDasNormas.get_inicio_vigencia(node_id)
            node["dataFimVigencia"] = ""  # se nao tem condicao  especial entao é o self.dataVigencia

            # se nao tem condicao  especial entao é o self.dataVigencia
            # padrao encontrado no grep que atingiu
            # 88,78% das normas   = 'vigor[a-z]* *na *data *de *sua *publica.*'

            # ainda não esta funcionando nessa versão
            # node["dataInicioVigencia"] = self.__tratamentoDasNormas.get_inicio_vigencia(node_id)

            # se nao tem condicao  especial entao é o self.dataVigencia

            # checa se a parte da norma se refere a inicio de vigencia, isso não ira pro banco mas precisará
            # de um tratamento posterior no banco para ajustar as demais partículas da norma de acordo com o excerto da
            # norma que fala sobre início de  vigência das demais

            # classe com os eventos possíveis no banco, retornando as alterações nos atributos de nos e
            # arestas necessários

            if 'p' in parte_articulacao:
                if (type(parte_articulacao['p']) == OrderedDict) and ('span' in parte_articulacao['p']):
                    print('//////////////////parte_articulacao[p]', parte_articulacao['p'])
                    # @todo precisa aperfeicoar isso, pq não esta pegando o texto dentro do span
                    node["div"] = parte_articulacao['p']['#text'] if '#text' in parte_articulacao['p'] else ' '
                else:
                    node["div"] = parte_articulacao['p']

        # checa se a parte da norma altera outra norma e chama o evento referente a alterações
        eventos_dados = EventosDados()

        if is_alteracao and key != 'Alteracao':
            '''
            ATENÇÃO: Importante dizer que analisando apenas o  .XML padrão LEXML é impossível dizer se o evento é uma 
            instituição ou evolução de partícula, a diferenciação entre um evento e outro só se pode dar através da 
            análise  se existe partícula anteriormente registrada no banco de dados. 
            '''

            # node_id =  re.sub("^.*?_alt[0-9]*_?", "", parte_articulacao['@id'])
            print('////////////parte_articulacao/////////////////////', parte_articulacao)

            # encontrou-se em normas muito antigas ex.: Decreto Lei 1629/1939 um padrão de alteração diferente do atual
            # o qual o parser-lexml nao identifica a parte alterada. Dado a isso foi necessário o if abaixo
            if '@xlink:href' not in parte_articulacao:
                parte_articulacao['@xlink:href'] = ''

            node["urnParticula"] = self.__xmlbase_alterada + "!" + parte_articulacao['@xlink:href'] + \
                                   "@" + self.__data_assinatura + ";" + "alteracao"

            urn_node_alterado = self.__xmlbase_alterada + "!" + parte_articulacao['@xlink:href']
            node["urnNorma"] = self.__xmlbase_alterada

            if 'p' in parte_articulacao:
                if (type(parte_articulacao['p']) == OrderedDict) and ('span' in parte_articulacao['p']):
                    node["div"] = parte_articulacao['p']['#text']
                else:
                    node["div"] = parte_articulacao['p']

            # monta as arestas necessárias para um evento de evolucao
            nodes_tratados = eventos_dados.set_evolucao_de_particulas(node,
                                                                      self.__xmlbase_alteradora,
                                                                      urn_node_alterado)
            for node_tratado in nodes_tratados:
                self.__tuplas.append(node_tratado)

        else:
            is_check_links_na_particula = self.__check_links_na_particula(parte_articulacao)
            # print('////////////////// is_check_links_na_particula', is_check_links_na_particula)
            # print('////////////////// is_check_links_na_particula', node['div'])

            # @todo rever, tentativa de corrigir eventuais erros
            if not is_check_links_na_particula:
                is_check_links_na_particula.append(parte_articulacao)

            if self.__tratamentoDasNormas.check_particula_revogacao(node['div']) or is_revogacao:  # @ if node['div]
                # is_revogacao = True  # @todo passar isso para  global?
                if not is_check_links_na_particula[
                           0] == parte_articulacao:
                    # para evitar metodo com retorno alternativo de tipos de variáveis, entao, se node é
                    # pq nao encontrou
                    nodes_tratados = eventos_dados.set_revogacao_de_particula(node, is_check_links_na_particula)

                    for node_tratado in nodes_tratados:
                        self.__tuplas.append(node_tratado)

            elif not is_check_links_na_particula[
                         0] == parte_articulacao:
                # para evitar metodo com retorno alternativo de tipos de variáveis,
                # entao, se node é pq nao encontrou
                associacao = AssociacaoSimples.associacao
                nodes_tratados = eventos_dados.set_relacao_entre_normas(node, is_check_links_na_particula,
                                                                        associacao,
                                                                        node["div"])
                for node_tratado in nodes_tratados:
                    self.__tuplas.append(node_tratado)

            # Tag Alteracao:  BaseURN = _from['@xml:base']+"!"+_from['@xlink:href']

        # cria o relacionamento com o no hierarquicamente superior.
        nodes_tratados = eventos_dados.set_relacao_hierarquia_particulas(node['urnParticula'], urn_no_superior)

        for node_tratado in nodes_tratados:
            self.__tuplas.append(node_tratado)
        return node

    def __localiza_posicao(self, node: dict) -> str:

        """
        monta a string relativa ao valor do atributo 'LocalizacaoNoTexto' , conforme o  subcapítulo  8.2.22 -
        Atributo : LocalizacaoNoTexto.

        :param node: dict - node atual
        :return: string - valor referente ao atributo.
        """
        '''
        Identificando localização no texto
        ## 1@1.1.1.1.1.1.1@1.1.1.1.1@12.2
        ## explicando
        ## 1                      @   1.1.1.1.1.1.1           @    1.1.1.1.1                  @12.2
        ## agrupamento principal  @  agrupamento secundario   @   agrupamento de articulacao  @ sequencia_textual

        ## agrupamento principal: parteInicial = 1, Articulacao = 2, ParteFinal = 3, Anexos=4 -
        ##agrupamento secundário: parte, livro, título, capítulo, seção ou subseção
        ##agrupamento de articulação: artigo, inciso, parágrafo, alínea, item, denominadores
        ##seqüência textual: sequência no texto
        '''

        node['familia'] = "parteInicial"
        localizacao_no_texto_anterior = self.__node_anterior['localizacaoNoTexto'].split('@')

        # agrupamento principal
        localizacao_no_texto_model = ArquiteturaDados.localizacao_no_texto_model
        agrupamento_principal = localizacao_no_texto_model['agrupamento_principal'][node["familia"]]

        # agrupamento secundario
        agrupamento_secundario = self.__get_sequencia_agrupamento_na_posicao(node, localizacao_no_texto_anterior[1],
                                                                             'agrupamento_secundario')

        agrupamento_secundario = [str(i) for i in agrupamento_secundario]
        agrupamento_secundario_str: str = '.'.join(agrupamento_secundario)

        # agrupamentoDeArticulacao
        agrupamento_de_articulacao = self.__get_sequencia_agrupamento_na_posicao(node, localizacao_no_texto_anterior[2],
                                                                                 'agrupamento_articulacao')
        agrupamento_de_articulacao = [str(i) for i in agrupamento_de_articulacao]
        agrupamento_de_articulacao_str: str = '.'.join(agrupamento_de_articulacao)

        # sequencia_textual
        if node["urnParticula"] == self.__node_anterior["urnParticula"]:
            sequencia_textual = int(localizacao_no_texto_anterior[3]) + 1
        else:
            sequencia_textual = 1
        '''
        print("\n\n ---------\n mandando agrupar "+ str(agrupamento_principal) + '@' + 
        str(agrupamento_secundario_str) + '@' + \
               str(agrupamento_de_articulacao_str) + '@' + str(sequencia_textual))

        getpass("Press Enter to Continue")
        '''

        return str(agrupamento_principal) + '@' + str(agrupamento_secundario_str) + '@' + \
               str(agrupamento_de_articulacao_str) + '@' + str(sequencia_textual)

    @staticmethod
    def __get_ementa(node: dict, urn_norma: str, div: str) -> dict:
        """
        Monta o nó  referente a ementa da Norma. No Documento de Arquitetura vide os subcapítulos 2.7.2.4  Família das
        Unidades da ParteInicial , 7.1 Padrão .nbex e JSON assim como o  ANEXO I – Exemplo de XML .nbex

        :param node: dict - nó do gênero ementa
        :param urn_norma:  str - urn da norma que esta sendo analisada ( a partícula de maior hierarquia)
        :param div: str - texto da ementa
        :return: dict - nó com a partícula do gênero ementa
        """
        node['urnParticula'] = urn_norma + "!ementa"
        node['familia'] = "parteInicial"
        node['genero'] = "ementa"
        node.pop("epigrafe")
        node['localizacaoNoTexto'] = "1@1.1.0.0.0.0.0@1.1.0.0.0@1.0"
        node['div'] = div
        return node

    @staticmethod
    def __get_preambulo(node: dict, urn_norma: str, div: str) -> dict:
        """
         Monta o nó  referente a preâmbulo da Norma. No Documento de Arquitetura vide os subcapítulos 2.7.2.4 Família
         das Unidades da ParteInicial , 7.1 Padrão .nbex e JSON assim como o  ANEXO I – Exemplo de XML .nbex

        :param node: nó do gênero  preâmbulo
        :param urn_norma: str - urn da norma que esta sendo analisada ( a partícula de maior hierarquia)
        :param div: str - texto do preâmbulo
        :return: dict - nó com a partícula do gênero preâmbulo
        """
        node['urnParticula'] = urn_norma + "!preambulo"
        node['familia'] = "parteInicial"
        node['genero'] = "ementa"
        if "epigrafe" in node:
            node.pop("epigrafe")
        node['localizacaoNoTexto'] = "1@2.1.0.0.0.0.0@1.1.0.0.0@0.0"
        # 1@1.1.0.0.0.0.0@1.1.0.0.0@1.0

        node['div'] = div
        return node

    def __parte_inicial(self, data_parte_inicial: dict):  # jsonFile
        """
        As normas são divididas entre parte Inicial ( da epigrafe até antes do primeiro artigo), articulação  e
        parte final ( após último elemento de artigulacao - artigo, paragrafo, inciso,), sendo a assinatura da
        autoridade, seu cargo, as referendas, etc... no banco elas são os valores das famílias da partículas da classe
        'partículas normativas', vide cap. 2.7.2 - em especial subcap 2.7.2.4 - do 'documento de arquitetura de dados':


        :param data_parte_inicial: parte do dict __json referente à parte inicial da norma
        :return: não há retorno, metodo adiciona nos em self.__tuplas
        """
        print('... tratando parte inicial da norma')

        # buscar_por = re.compile('@.*', re.MULTILINE)
        # urn_norma = re.sub(buscar_por, '', urn_norma)
        arr_urn_norma = [x.strip() for x in self.__xmlbase.split(':')]
        self.__nodeModel['urnNorma'] = self.__xmlbase
        self.__nodeModel['ordem'] = arr_urn_norma[4]
        self.__ordem = arr_urn_norma[4]
        self.__nodeModel['autoridadeOrigem'] = arr_urn_norma[3]
        self.__nodeModel['hieraquiaNormasNivel'] = ArquiteturaDados.ordem.index(self.__nodeModel['ordem']) \
            if self.__nodeModel['ordem'] in ArquiteturaDados.ordem else len(ArquiteturaDados.ordem)
        self.__nodeModel['genero'] = 'Norma'
        self.__nodeModel['dataAssinatura'] = self.__data_assinatura

        # identificando inicio de vigencia
        print('... identificando inicio de vigência das partículas ')
        self.__tratamentoDasNormas.set_inicio_vigencia([self.__json['LexML']['ProjetoNorma']['Norma']['Articulacao']],
                                                       self.__json['LexML']['ProjetoNorma']['Norma']['Articulacao'])

        self.__nodeModel["dataInicioVigencia"] = self.__tratamentoDasNormas.get_inicio_vigencia('Norma')
        self.__nodeModel["dataPublicacao"] = self.__data_publicacao

        node_inicial = self.__nodeModel.copy()
        node_inicial['urnParticula'] = self.__xmlbase + "!norma"
        node_inicial['familia'] = ArquiteturaDados.familia[node_inicial['genero']]
        node_inicial['epigrafe'] = data_parte_inicial['Epigrafe']["#text"]

        self.__tuplas.append(node_inicial)
        self.__node_anterior = node_inicial
        # tratando ementa
        # tem que iterar daqui pra frente

        for key in data_parte_inicial:
            if key == "Ementa":
                node_ementa = node_inicial.copy()
                node_ementa = self.__get_ementa(node_ementa, node_inicial['urnNorma'],
                                                data_parte_inicial['Ementa']['#text'])
                self.__tuplas.append(node_ementa)
            elif key == "Preambulo":
                node_preambulo = self.__tuplas[0].copy()
                print('... tratando de preambulo')
                texto_preambulo = data_parte_inicial['Preambulo']['p'] if 'p' in data_parte_inicial['Preambulo'] else ''

                if type(texto_preambulo) == list:
                    texto_preambulo = [x for x in texto_preambulo if x is not None]
                    texto_preambulo = ' '.join(texto_preambulo)

                node_preambulo = self.__get_preambulo(node_preambulo, node_inicial['urnNorma'],
                                                      texto_preambulo)
                self.__tuplas.append(node_preambulo)

    def __parte_articulacao(self, data_parte_articulacao: list, no_hierarquic_superior: str, level=0,
                            is_alteracao=False, is_revogacao=False, has_omissis=False):
        """
         Tratando parte Articuladora - As normas são divididas entre parte Articulação (do primeiro artigo até a última
         particula de classenormativa), vide cap. 2.7.2 - em especial subcap 2.7.2.2 -
         do 'documento de arquitetura de dados':

        :param data_parte_articulacao: list - list contendo OrderedDicts referentes a parte de articulação das normas
        :return: não há retorno, metodo adiciona nos em self.__tuplas
        """

        # data_parte_articulacao = [data_parte_articulacao]
        print('... tratando: ', no_hierarquic_superior)

        for articulacoes in data_parte_articulacao:
            no_hierarquic_inferior = no_hierarquic_superior

            if type(articulacoes) == OrderedDict:
                for i, (key, value) in enumerate(articulacoes.items()):
                    if key in ArquiteturaDados.genero or key == 'Alteracao':
                        if key == 'Alteracao':
                            self.__xmlbase_alteradora = no_hierarquic_superior
                            print(value)
                            self.__xmlbase_alterada = value['@xml:base'] if '@xml:base' in value \
                                else self.__xmlbase_alterada
                            is_alteracao = True

                        if type(value) == OrderedDict:
                            no_hierarquic_inferior = self.__xmlbase + "." + value['@id']
                            no_hierarquic_superior = 'Norma' if level == 0 else no_hierarquic_superior
                            print('value', value)
                            node_normativo = self.__monta_particula(
                                value,  # parte_articulacao
                                key,  # key
                                no_hierarquic_superior,  # no_hierarquic_superior
                                is_alteracao,  # , is_alteracao = False
                                is_revogacao
                            )
                            """
                            if 'Omissis' not in value:
                                node_normativo = self.__monta_particula(
                                    value,  # parte_articulacao
                                    key,  # key
                                    no_hierarquic_superior,  # no_hierarquic_superior
                                    is_alteracao,  # , is_alteracao = False
                                    is_revogacao
                                )
                            """
                            # Alteracao nao tem id , nem é um no em si mesmo
                            if not key == ('Alteracao' or 'Omissis'):
                                self.__node_anterior = node_normativo
                                self.__tuplas.append(node_normativo)

                            self.__parte_articulacao([value],
                                                     no_hierarquic_inferior,
                                                     level + 1,
                                                     is_alteracao,
                                                     is_revogacao)
                            # is_alteracao = False
                        if type(value) == list and all(isinstance(x, OrderedDict) for x in value):

                            subitens_iguais = []
                            for particula in value:
                                subitens_iguais.append(OrderedDict([(key, particula)]))
                            self.__parte_articulacao(subitens_iguais,
                                                     no_hierarquic_inferior,
                                                     level,
                                                     is_alteracao,
                                                     is_revogacao)
                            # is_alteracao = False
            # is_alteracao = False
            # is_revogacao = False

    def __parte_final(self, data_parte_final: dict):
        """
        Quando existir no Lexml, trata a parte final capiturando local, data de assinatura , signatários e referendas
        :param self:
        :param data_parte_final:
        :return:
        """
        '''       
        <ParteFinal>
            <LocalData>Paço Municipal, 20 de setembro de 2000.</LocalData>
            <Assinatura>
                <AssinaturaNome>Francisco Amaral</AssinaturaNome>
                <AssinaturaCargo>Prefeito Municipal</AssinaturaCargo>
            </Assinatura>     
        </ParteFinal>
        '''
        self.__nodeModel['local'] = self.__tratamentoDasNormas.get_local_assinatura(data_parte_final['LocalData'])

        for key, assinatura in data_parte_final:
            if key == 'LocalData':
                continue
            elif key == 'Assinatura':
                node_signatario_fisica = self.__node_pessoas_fisicas.copy()
                '''
                    node_pessoa_fisica_model = {'codAutoridade': '', 
                                                'cpfAutoridade': '', 
                                                'nome': '', 
                                                'siape': ''}
                '''
                node_signatario_fisica['nome'] = assinatura['AssinaturaNome']
                node_conexao_assinatura = ArquiteturaDados.law_link_model['assinatura'].copy()
                node_conexao_assinatura['_to'] = assinatura['AssinaturaNome']
                node_conexao_assinatura['cargoAutoridade'] = assinatura['AssinaturaCargo']

                self.__aresta_assinaturas.update({len(self.__aresta_assinaturas): node_conexao_assinatura})

    def fit_directory(self, directory=".") -> bool:
        """
               Itera com todos os arquivos .xml (lexml  do diretório  apontado no parametero  e faz a conversoã para
               .nbex

               :param directory: diretório onde se encontram os arquivos a serem parseados

               :return: bool
        """

        counter_sucess: int = 0
        if not os.path.isdir(directory):
            return False

        for file in os.listdir(directory):
            print('...', counter_sucess, ' arquivos convertidos')
            counter_sucess += 1

            if file.endswith(".xml"):
                filepath = directory + '/' + file
                print('... convertendo o arquivo', file)
                self.fit_norma(filepath)
                resultado = self.tuplas
                namefileresult = file.split('.')
                fileresult = directory + '/nbex/' + namefileresult[0] + '.nbex'

                if not os.path.isdir(directory + '/nbex'):
                    os.mkdir(directory + '/nbex')

                try:
                    with open(fileresult, 'w') as f:
                        f.write(str(resultado))
                    f.close()
                except SystemError:
                    print('não foi possível converter ou salvar o arquivo', fileresult)
        return True

    def fit_norma(self, filename: str, filetype="xml"):
        """
        Metodo chamado para gerar todos os nos da norma contida no XML

        :param filename: nome do arquivo xml para parsear
        :param filetype:
        :return: sem retorno
        """
        if not os.path.isfile(filename):
            print('//////////////// Arquivo ' + filename + ' não encontrado!')
            return False

        print('convertendo norma a partir do arquivo', filename)

        self.__set_json(filename, filetype)
        self.__tuplas = []

        # driver dos demais
        # tratar parte inicial, precisa iteira-la
        self.__parte_inicial(self.__json['LexML']['ProjetoNorma']['Norma']['ParteInicial'])
        if 'ParteFinal' in self.__json['LexML']['ProjetoNorma']['Norma']:
            self.__parte_final(self.__json['LexML']['ProjetoNorma']['Norma']['ParteFinal'])
        else:
            self.__nodeModel['local'] = 'Brasília'
        articulacao = self.__json['LexML']['ProjetoNorma']['Norma']['Articulacao']
        self.__parte_articulacao([articulacao], self.__xmlbase)

        goodbye = """  
        ****************************************************************
        Conversão da norma de formato LEXLM -> NBEX (Codex) concluída 
        -------------------------------------------------
        
        Será necessária, agora, a população do banco e a revisão e validação dos nós e arestas estruturados.
        
        Para visualizar todos os nós e arestas. ex : 
         
        >>> testeCodex = conversor_codex.CodexXMLtoDB()
        >>> testeCodex.fit_norma('decreto_9094_2017.xml')

        pegar os nós:
        >>> testeCodex.tuplas_nodes

        pegaras arestas: 
        >>> testeCodex.tuplas_edges

        pegar todos (nós e arestas)
        >>> testeCodex.tuplas


        Para mais informações sobre como usar esta classe digite CodexXMLtoDB().help()
        
        Para mais informaçẽos sobre regras de negócio leia o Documento Arquitetura_e_Comportamento_de_Dados_Codex v0.1.8
        ****************************************************************
        """
        print('\n', goodbye, '\n')

    def tuplas_edges(self, tipo='geral'):
        """

        :param tipo: geral, externo , interno
        :return:
        """
        tipologia = ArquiteturaDados.type_edges
        arestas = []
        for value in self.__tuplas:
            if value['tipoJson'] == 'aresta' and tipo in tipologia[tipo]:
                arestas.append(value)
        return arestas

# @todo nos do tipo alteracao ex.:
# {'tipoJson': 'node',  'urnParticula': 'urn:lex:br:federal:decreto:2004;4992!art6@2004-03-31;alteracao',
# estão se repetindo


# @todo tratar omissis omissis


# @todo localizacao no texto da nova particula, na lei antiga,  no caso de instituicao

# @todo particulaReferenteAInicioVigencia : True
# @todo identificar uma revogação e verificar pra baixo
# @todo verificar a questao das revogações (aresta:'encerraVigencia')
# @todo inseir aprimoramento de redes neurais conforme os cadernos jupyter
# @todo fazer um metodo get da versão
# @todo inserir o nome do  arquivo de origem na particula normativa

# @todo a posicao da particula no texto nao esta saindo adequadamente
# @todo HYPERLINK NAO SAIU
# @todo colocar como regra  o inicio de vigencia como o descrito no art 1 da LINDB (45 dias pos publicacao)
# @todo aparentemente nao esta salvando as relacoes do tipo associacao.
