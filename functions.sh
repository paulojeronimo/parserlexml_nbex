#!/usr/bin/env bash
cd "$(dirname "${BASH_SOURCE[0]}")"
export PLN_DIR=`pwd -P` # <- pln: "parser" "lexml" "nbex"
if ! source ./env-variables.sh &> /dev/null
then
	source ./env-variables.sample.sh
fi
cd "$OLDPWD"

pln-cd() { 
	cd "$PLN_DIR"
}

pln-venv-create() {
	if ! [ -d venv ]
	then
		echo "Creating the virtual environment ($PLN_DIR/venv) ..."
		python3 -m venv venv
		pln-venv-activate
		echo "Upgrading pip ..."
		pip3 install --upgrade pip
		echo "Installing the Python requirements ..."
		pip3 install -r requirements.txt
	else
		echo "\"$PLN_DIR/venv\ already exists!"
	fi
}

pln-venv-activate() {
	if ! [ "${VIRTUAL_ENV:-}" ] || [ "$VIRTUAL_ENV" != "$PLN_DIR"/venv ]
	then
		echo "Activating the virtual environment ($PLN_DIR/venv) ..."
		pushd "$PLN_DIR" > /dev/null
		[ -d venv ] || {
			echo "Could not activate!"
			return 1
		}
		source ./venv/bin/activate 2>&- || {
			echo "Could not activate!"
			return 1
		}
		popd > /dev/null
	else
		echo "\"$PLN_DIR/venv\" already activated!"
	fi
}

pln-venv-deactivate() {
	deactivate
}

# NOTE: this is an internal function (note the convention use of "_" before the name)
_pln-create-list-of-directories() {
	local t=${1:-slice}
	local f=$DATA_DIR/lista-diretorios-html.$t.csv
	if [ -f "$f" ]
	then
		cp "$f" "$DATA_DIR/$HTM_DIR/$CSV_FILE"
	else
		echo "File \"$f\" not found!"
	fi
}

pln-use-slice-list-of-directories() {
	_pln-create-list-of-directories
}

pln-use-full-list-of-directories() {
	_pln-create-list-of-directories full
}

pln-cat-list-of-directories() {
	cat "$DATA_DIR"/$HTM_DIR/$CSV_FILE
}

case "$OSTYPE" in
  darwin*)
    which gsed &> /dev/null || {
      echo -e "Install gsed!\n$ brew install gnu-sed"
      return 1
    }
    sed() { gsed "$@"; }
    which ggrep &> /dev/null || {
      echo -e "Install ggrep!\n$ brew install grep"
      return 1
    }
    function grep { ggrep "$@"; }
esac
type jdk &> /dev/null && jdk 11
profile=~/.bash_profile
[[ $OSTYPE =~ ^linux ]] && profile=~/.bashrc
functions_sh=$PLN_DIR/functions.sh
if ! [ -f $profile ] || ! grep -q "^source \"$functions_sh\"$" $profile; then
  echo "source \"$functions_sh\"" >> $profile
  echo "'source \"$functions_sh\"' added to $profile"
fi
